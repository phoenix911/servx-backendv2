class DropAndAddAttributesToVehicles < ActiveRecord::Migration
  def change
  	add_reference :vehicles, :vehiclemodel, index: true, foreign_key: true
  	remove_column :vehicles, :manufacturer_id
  	remove_column :vehicles, :model
  	remove_column :vehicles, :image
  	rename_column :vehicles, :registration_number, :reg_num
  end
end
