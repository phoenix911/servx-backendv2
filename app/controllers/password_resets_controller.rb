class PasswordResetsController < ApplicationController
  
  # POST /password_resets
  # POST /password_resets.json
  def create
    @user = User.find_by(mobile: password_reset_params[:mobile])
    respond_to do |format|
      if @user
      	@user.send_otp_from_controller
      	format.json {render json:{ code: 1, msg: "You will receive your OTP shortly."}}
      else
        format.json {render json:{ code: 0, msg: "Account does not exist."}}
    	end
    end 
  end

  def update
    if password_reset_params[:password].nil? || password_reset_params[:password].blank?
      respond_to do |format|
        format.json { render json:{ code: 0, msg: "Password can't be blank.", hash: 0 }}
      end
      return
    end
    @user = User.find_by(mobile: password_reset_params[:mobile])
    respond_to do |format|
      if @user.otp==password_reset_params[:otp].to_i
        if !@user.activated?
          @user.activate         
        end       
        if @user.update_attributes(password_reset_params.except(:remember_digest,:otp))
          @user.update_multiple_values
          format.json { render json:{ code: 1, msg: "password updated.", user: @user.as_json(only: ['name','id','remember_digest','mobile','email','address']), hash: 0}}
        else
          format.json { render json:{ code: 0, msg: "Password can't be less than 6 characters", hash: 0 }}
        end
      else
          format.json {render json:{ code: 0, msg: "OTP doesn't match.", hash: 0}}
      end 
    end         
	end 

  def resend
    @user = User.find_by(mobile: password_reset_params[:mobile])
    @user.send_otp_from_controller
    respond_to do |format|
      if @user
        format.json {render json:{ code: 1, msg: "You will receive your OTP shortly."}}
      else
        format.json {render json:{ code: 0, msg: "User doesn't exist."}}
      end
    end
  end 

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def password_reset_params
      params.permit(:mobile, :password, :otp, :remember_digest)
    end
end
