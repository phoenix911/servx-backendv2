class PackageVehicleModelsController < ApplicationController
  before_action :set_package_vehicle_model, only: [:show, :edit, :update, :destroy]

  # GET /package_vehicle_models
  # GET /package_vehicle_models.json
  def index
    @package_vehicle_models = PackageVehicleModel.all
  end

  # GET /package_vehicle_models/1
  # GET /package_vehicle_models/1.json
  def show
  end

  # GET /package_vehicle_models/new
  def new
    @package_vehicle_model = PackageVehicleModel.new
  end

  # GET /package_vehicle_models/1/edit
  def edit
  end

  # POST /package_vehicle_models
  # POST /package_vehicle_models.json
  def create
    @package_vehicle_model = PackageVehicleModel.new(package_vehicle_model_params)

    respond_to do |format|
      if @package_vehicle_model.save
        format.html { redirect_to @package_vehicle_model, notice: 'Package vehicle model was successfully created.' }
        format.json { render :show, status: :created, location: @package_vehicle_model }
      else
        format.html { render :new }
        format.json { render json: @package_vehicle_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /package_vehicle_models/1
  # PATCH/PUT /package_vehicle_models/1.json
  def update
    respond_to do |format|
      if @package_vehicle_model.update(package_vehicle_model_params)
        format.html { redirect_to @package_vehicle_model, notice: 'Package vehicle model was successfully updated.' }
        format.json { render :show, status: :ok, location: @package_vehicle_model }
      else
        format.html { render :edit }
        format.json { render json: @package_vehicle_model.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /package_vehicle_models/1
  # DELETE /package_vehicle_models/1.json
  def destroy
    @package_vehicle_model.destroy
    respond_to do |format|
      format.html { redirect_to package_vehicle_models_url, notice: 'Package vehicle model was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_package_vehicle_model
      @package_vehicle_model = PackageVehicleModel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def package_vehicle_model_params
      params.require(:package_vehicle_model).permit(:vehiclemodel_id, :package_id)
    end
end
