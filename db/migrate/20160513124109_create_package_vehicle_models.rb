class CreatePackageVehicleModels < ActiveRecord::Migration
  def change
    create_table :package_vehicle_models do |t|
      t.references :vehiclemodel, index: true, foreign_key: true
      t.references :package, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
