class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.string :name
      t.integer :cost
      t.boolean :all, default: :false

      t.timestamps null: false
    end
  end
end
