class Job < ActiveRecord::Base
	has_many :vehiclemodels, through: :prices
	has_many :prices, dependent: :destroy
	has_many :packages, through: :job_packages
	has_many :job_packages, dependent: :destroy
	default_scope -> { order(:name) }
end
