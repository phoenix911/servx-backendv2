class Offer < ActiveRecord::Base
	attr_accessor :type	
	has_many :bookings	
	has_many :dealerships, through: :getoffers	
	has_many :getoffers, dependent: :destroy
	mount_uploader :image, ImageUploader
	validate  :image_size


	def add_manufacturers(manufacturers)
		manufacturers= eval(manufacturers)
		manufacturers.each do |manufacturer|
			dealerships= Dealership.where(manufacturer_id: manufacturer)
			dealerships.each do |dealership|
				Getoffer.create!(offer_id: self[:id], dealership_id: dealership[:id])
			end
		end
	end

	def add_dealerships(dealerships)
		dealerships= eval(dealerships)
		dealerships.each do |dealership|
			Getoffer.create!(offer_id: self[:id], dealership_id: dealership)
		end
	end

	def deactivate
		update_attribute(:deactivated, true)
	end

  private

     def image_size
      if image.size > 5.megabytes
        errors.add(:image, "Image size should be less than 5 MB")
      end
    end
end
