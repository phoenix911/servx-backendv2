class VehiclesController < ApplicationController
  before_action :set_vehicle,           only: [:update, :destroy]
  before_action :belongs_to_user_id,    only: [:update, :destroy]
  before_action :logged_in_user,        only: [:create, :update, :destroy, :vehicle_bookings, :get_dealers]
  before_action :belongs_to_user,       only: [:vehicle_bookings, :get_dealers]

  # GET /vehicles
  # GET /vehicles.json
  # this will be for admin
  # def index
  #   @vehicles = Vehicle.all
  #   respond_to do |format|
  #     format.json { render json:{ code: "1", msg: "Showing all vehicles.", vehicles: @vehicles }}
  #   end
  # end

  # # GET /vehicles/1
  # # GET /vehicles/1.json
  # def show
  #   respond_to do |format|
  #     format.json { render json:{ code: "1", msg: "Showing vehicle.", vehicle: @vehicle }}
  #   end
  # end

  # POST /vehicles
  # POST /vehicles.json
  def create
    vehicle_params[:registration_number].gsub!(/\s+/, '') unless vehicle_params[:registration_number].nil?
    vehicle_params[:registration_number].upcase! unless vehicle_params[:registration_number].nil?
    @vehicle = Vehicle.new(vehicle_params.except(:remember_digest, :location, :vehicle_id, :deactivated))
    if !@vehicle.model.nil?
      @vehicle.update_image(@vehicle.model)
    end    
    respond_to do |format|
      if @vehicle.save
        @user.last_active_update
        format.json { render json:{ code: 1, msg: "Added #{@vehicle.model}", vehicle: @vehicle.as_json(except: ['created_at','updated_at','user_id','manufacturer_id','deactivated']), hash: 0 }}
      else
        format.json { render json:{ code: 0, msg: @vehicle.errors.messages, hash:1 }}
      end
    end
  end

  # PATCH/PUT /vehicles/1
  # PATCH/PUT /vehicles/1.json
  def update
    vehicle_params[:registration_number].gsub!(/\s+/, '') unless vehicle_params[:registration_number].nil?
    vehicle_params[:registration_number].upcase! unless vehicle_params[:registration_number].nil?
    respond_to do |format|
      if @vehicle.update_attributes(vehicle_params.except(:remember_digest, :location, :vehicle_id, :user_id, :deactivated))
        @vehicle.update_image(@vehicle.model)        
        format.json { render json:{ code: 1, msg: "successfully updated.", vehicle: @vehicle.as_json(except: ['created_at','updated_at','user_id','manufacturer_id','deactivated'])}}
      else
        format.json { render json:{ code: 0, msg: @vehicle.errors.messages }}
      end
    end
  end

  # DELETE /vehicles/1
  # DELETE /vehicles/1.json
  def destroy
    name=@vehicle.model
    @vehicle.deactivate
    respond_to do |format|
      format.json { render json:{ code: 1, msg: "Removed #{name}." }}
    end
  end

  def vehicle_bookings
    bookings= @vehicle.bookings
    respond_to do |format|
      if !bookings.empty?
        format.json { render json:{ code: 1, msg: "Showing all bookings of your selected vehicles.", bookings: bookings }}
      else
        format.json { render json:{ code: 0, msg: "No bookings present." }}
      end  
    end
  end  

  def get_dealers
    location= Location.find_by(name: vehicle_params[:location].titleize) unless vehicle_params[:location].nil? 
    if location.nil?
      respond_to do |format|
        format.json { render json:{ code: 0, msg: "servX is coming to a dealership near you soon. Apologies for the inconvenience caused." }}
      end  
      return
    end
    alldealers=location.dealerships
    if alldealers.nil?
      respond_to do |format|
        format.json { render json:{ code: 0, msg: "servX is coming to a dealership near you soon. Apologies for the inconvenience caused." }}
      end  
      return
    end      
    dealers=alldealers.where(manufacturer_id: @vehicle.manufacturer_id)
    respond_to do |format|
      if dealers.empty?
        format.json { render json:{ code: 0, msg: "servX is coming to a dealership near you soon. Apologies for the inconvenience caused." }}
      else
        ratings= Array.new
        dealers.each do |dealer|
          rating= dealer.get_rating
          if rating==0.0
            ratings.push("N/A")
          else
            ratings.push(rating)
          end
        end
        manufacturer = Manufacturer.find_by(id: @vehicle.manufacturer_id)
        image = manufacturer.image
        format.json { render json:{ code: 1, msg: "Showing all dealerships.", dealers: dealers.as_json(only: ['image','latitude','longitude','address', 'name', 'id']), ratings: ratings , logo: image}}
      end
    end    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find_by(id: params[:vehicle_id])
      if @vehicle.nil? || @vehicle.deactivated==true 
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Vehicle not found." }}
        end
      end      
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vehicle_params
      params.permit(:manufacturer_id, :model, :registration_number, :user_id, :remember_digest, :location, :vehicle_id, :deactivated)
    end
    # Check if user is same as vehicle's user_id using vehicle id.
    def belongs_to_user_id
      if @vehicle.user_id!=params[:user_id].to_i
        respond_to do |format|         
          format.json { render json:{ code: 0, msg: "Invalid access. This vehicle doesn't belong to you." }}
        end
      end     
    end
    # Check if user is same as vehicle's user_id.
    def belongs_to_user
      #try using id here if possible
      # vehicle_params[:registration_number].gsub!(/\s+/, '') unless vehicle_params[:registration_number].nil?
      # @vehicle= Vehicle.find_by(registration_number: vehicle_params[:registration_number])
      @vehicle= Vehicle.find_by(id: vehicle_params[:vehicle_id])    
      if @vehicle.nil? || @vehicle.deactivated==true
        respond_to do |format|           
          format.json { render json:{ code: 0, msg: "Vehicle not found." }} 
        end       
      elsif @vehicle.user_id!=vehicle_params[:user_id].to_i
        respond_to do |format|         
          format.json { render json:{ code: 0, msg: "Invalid access. This vehicle doesn't belong to you." }}
        end
      end         
    end     
end