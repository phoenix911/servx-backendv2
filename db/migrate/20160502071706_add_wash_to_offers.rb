class AddWashToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :wash, :boolean, default: :false
  end
end
