class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.booking_detail.subject
  #

  def welcome_email(user)
    @user = user

    require 'mailgun'
    # PUT API-KEY IN ENVIRONMENT VARIABLE FILE!!!keeping it open for development right now... but ServX's repository is private...so...
    # First, instantiate the Mailgun Client with your API key
    mg_client = Mailgun::Client.new "key-792d235a48f4513679ff561935d44bb5"

   html = render_to_string template: "user_mailer/welcome_email.html.erb"
    text= render_to_string template: "user_mailer/welcome_email.text.erb"
    # Define your message parameters
    message_params = {:from    => 'ServX <contact@servx.in>',  
                      :to      => @user.email,
                      :subject => 'Welcome to ServX!',
                      :text    => text.to_str,
                      :html    => html.to_str}

    # Send your message through the client
    mg_client.send_message "connx.in", message_params    
  end


  def booking_detail(booking,user,vehicle,dealership,manufacturer,vehiclemodel)
    @booking      =  booking
    @user         =  user
    @vehicle      =  vehicle
    @dealership   =  dealership
    @manufacturer =  manufacturer
    @bookingarray =  @booking.jobs.split('#')
    @counter      =  0
    @vehiclemodel =  vehiclemodel
    # @package      =  package
    # @offer        =  offer
    require 'mailgun'
    # PUT API-KEY IN ENVIRONMENT VARIABLE FILE!!!keeping it open for development right now... but ServX's repository is private...so...
    # First, instantiate the Mailgun Client with your API key
    mg_client = Mailgun::Client.new "key-792d235a48f4513679ff561935d44bb5"

   html = render_to_string template: "user_mailer/booking_detail.html.erb"
    text= render_to_string template: "user_mailer/booking_detail.text.erb"
    # Define your message parameters
    message_params = {:from    => 'ServX <contact@servx.in>',  
                      :to      => @user.email,
                      :subject => 'Booking Notification',
                      :text    => text.to_str,
                      :html    => html.to_str}

    # Send your message through the client
    mg_client.send_message "connx.in", message_params    
  end

  def booking_detail_to_admin(booking,user,vehicle,dealership,vehiclemodel)
    @booking    = booking
    @user       = user
    @vehicle    = vehicle
    @dealership = dealership
    @vehiclemodel =  vehiclemodel
    require 'mailgun'
    # PUT API-KEY IN ENVIRONMENT VARIABLE FILE!!! keeping it open for development right now.
    # First, instantiate the Mailgun Client with your API key
    mg_client = Mailgun::Client.new "key-792d235a48f4513679ff561935d44bb5"

   html = render_to_string template: "user_mailer/booking_detail_to_admin.html.erb"
    text= render_to_string template: "user_mailer/booking_detail_to_admin.text.erb"
    # Define your message parameters
    message_params = {:from    => 'ServX <contact@servx.in>',  
                      :to      => 'anubhav@servx.in',
                      :cc      => 'shubham@servx.in, akansh@servx.in, gaurav@servx.in',
                      :subject => 'Booking Notification',
                      :text    => text.to_str,
                      :html    => html.to_str}

    # Send your message through the client
    mg_client.send_message "connx.in", message_params    
  end  

end
