json.array!(@prices) do |price|
  json.extract! price, :id, :vehiclemodel_id, :job_id, :price
  json.url price_url(price, format: :json)
end
