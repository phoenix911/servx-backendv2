class ChangeActivatedFormatInDealership < ActiveRecord::Migration
  def change
  	change_column :dealerships, :activated, :boolean, default: false
  end
end
