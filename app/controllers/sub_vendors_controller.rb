class SubVendorsController < ApplicationController
  before_action :logged_in_sub_vendor, only: [:signout, :bookings, :booking, :update_booking, :assign_pickup_driver, :assign_drop_driver, :drivers, :driver_bookings, :make_offer]
  before_action :check_booking,    only: [:booking, :update_booking, :assign_pickup_driver, :assign_drop_driver]
  before_action :check_status,     only: [:update_booking] 
  before_action :check_driver,     only: [:assign_pickup_driver, :assign_drop_driver, :driver_bookings]
  before_action :check_booking_compatibilty_with_driver,     only: [:assign_pickup_driver, :assign_drop_driver]  
  before_action :check_fields,     only: [:make_offer]    
  # GET /sub_vendors
  # GET /sub_vendors.json
  def index
    @sub_vendors = SubVendor.all
  end

  # POST /sub_vendors
  # POST /sub_vendors.json
  def signin
    @sub_vendor= SubVendor.find_by(mobile: sub_vendor_params[:mobile])
    respond_to do |format|
      if @sub_vendor && @sub_vendor.authenticate(sub_vendor_params[:password])
        # @sub_vendor.update_remember
        # @sub_vendor.last_active
        @sub_vendor.update_mutiple_values
        format.json { render json: {code: 1, msg: "Successfully logged in!", sub_vendor: @sub_vendor.as_json(only: ['name','id','remember_digest','mobile','email','image'])}}
      elsif @vendor
        format.json { render json: {code: 0, msg: "Invalid password"}}
      else
        format.json { render json: {code: 0, msg: "Invalid email/password combination."}}
      end
    end  
  end

  def signout
    @sub_vendor.forget
    respond_to do |format|
      format.json { render json: {code: 1, msg: "Successfully logged out."}}       
    end
  end
  
  def bookings
    bookings=Booking.where(dealership_id: @sub_vendor[:dealership_id]).order('id DESC')
    @sub_vendor.last_active
    respond_to do |format|
      format.json { render json:{ code: 1, bookings: bookings.as_json(include: {user: {only: ['id','name','mobile',"address"]},
        dealership: {only: ['id','name','address','image']},vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},
        only: ['reference_code','jobs','pickup_time','drop_time', 'status', 'cost', 'id'])}}       
    end    
  end  

  def booking
    @booking.change_job_format
    respond_to do |format|
        pickup_driver=Driver.find_by(id: @booking[:pickup_driver])
        drop_driver=Driver.find_by(id: @booking[:drop_driver])
        if pickup_driver && drop_driver
          format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
            vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
            pickup_driver: pickup_driver.as_json(only: ['id', 'name', 'mobile', 'image']), drop_driver: drop_driver.as_json(only: ['id', 'name', 'mobile', 'image'])}}
        elsif pickup_driver
          format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
            vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
            pickup_driver: pickup_driver.as_json(only: ['id', 'name', 'mobile', 'image'])}}
        elsif drop_driver
          format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
            vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
            drop_driver: drop_driver.as_json(only: ['id', 'name', 'mobile', 'image'])}} 
        else
          format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
            vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}}, only: ['reference_code','jobs','pickup_time', 'status', 'cost'])}} 
        end           
    end   
  end

  def update_booking
    respond_to do |format|
     sub_vendor_params[:status]= Booking.statuses[sub_vendor_params[:status]]
      if @booking.update_columns(sub_vendor_params.slice(:status, :pickup_time, :jobs, :cost))
        if sub_vendor_params[:status]=="Your vehicle has reached the service centre"
          if !@booking.pickup_driver.nil?
            driver=Driver.find_by(@booking.pickup_driver)
            if driver
              driver.update_status(1)
            end
          end 
          if driver
            format.json { render json:{ code: 1, msg: "updated booking. Updated #{driver.name}'s status with available", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}               
          else
            format.json { render json:{ code: 1, msg: "updated booking.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}               
          end
        elsif sub_vendor_params[:status]=="Payment received"
          if !@booking.drop_driver.nil?
            driver=Driver.find_by(@booking.drop_driver)
            if driver
              driver.update_status(1)
            end
          end 
          if driver
            format.json { render json:{ code: 1, msg: "updated booking status. Updated #{driver.name}'s status with available", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}               
          else
            format.json { render json:{ code: 1, msg: "updated booking.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}                         
          end
        else
          format.json { render json:{ code: 1, msg: "updated booking.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}  
        end
      else
        format.json { render json:{ code: 0, msg: @booking.errors.messages }}
      end  
    end     
  end  

  def assign_pickup_driver
    respond_to do |format|
      if sub_vendor_params[:driver_id]== "0" || sub_vendor_params[:driver_id]== 0
        if !@booking[:pickup_driver].nil?
          driver=Driver.find_by(id: @booking[:pickup_driver])
          if driver
            driver.update_status(1)
          end
        end
        @booking.update_pickup_driver(nil)
        format.json { render json:{ code: 1, msg: " Pick up driver removed." }}        
      else 
        @booking.update_pickup_driver(sub_vendor_params[:driver_id].to_i)
        @driver.update_status(0)
        format.json { render json:{ code: 1, msg: "Driver assigned to booking with reference code: #{@booking.reference_code} for pickup." }}
      end
    end
  end

  def assign_drop_driver
    respond_to do |format|
      if sub_vendor_params[:driver_id]== "0" || sub_vendor_params[:driver_id]== 0
        if !@booking[:drop_driver].nil?
          driver=Driver.find_by(id: @booking[:drop_driver])
          if driver
            driver.update_status(1)
          end
        end
        @booking.update_drop_driver(nil)
        format.json { render json:{ code: 1, msg: " Drop driver removed." }}        
      else 
        @booking.update_drop_driver(sub_vendor_params[:driver_id].to_i)
        @driver.update_status(0)
        format.json { render json:{ code: 1, msg: "Driver assigned to booking with reference code: #{@booking.reference_code} for drop." }}
      end
    end
  end

  def drivers
    @drivers= Driver.where('deactivated = ? AND dealership_id = ?', :false, @sub_vendor[:dealership_id])
    respond_to do |format|
      format.json { render json:{ code: 1, msg: "Listing all drivers of this dealership", drivers: @drivers.as_json(only: ['id', 'name', 'image', 'mobile', 'available']) }}
    end
  end   

  def driver_bookings
    bookings=Booking.where('pickup_driver = ? OR drop_driver = ?', sub_vendor_params[:driver_id], sub_vendor_params[:driver_id])
    respond_to do |format|
      if !bookings.empty?
        format.json { render json: {code: 1, msg: "Showing all bookings assigned to this driver.", bookings: bookings}}      
      else
        format.json { render json: {code: 1, msg: "No bookings found."}} 
      end
    end  
  end

  def make_offer
    @dealership= @sub_vendor.dealership
    respond_to do |format|
      if @dealership
          SubVendorMailer.offer_mail(@dealership,sub_vendor_params[:offer_name],sub_vendor_params[:terms],sub_vendor_params[:description],sub_vendor_params[:expires_at]).deliver_now
          format.json { render json: {code: 1, msg: "A mail has been sent to one of our executives. We will contact you shortly."}}
        else
          format.json { render json: {code: 1, msg: "Dealership not found."}}  
        end      
    end   
  end  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sub_vendor
      @sub_vendor = SubVendor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sub_vendor_params
     @sub_vendor_params ||= params.permit(:name, :email, :password, :remember_digest, :mobile, :vendor_id, :booking_id, :status, :pickup_time, :jobs, :cost, :driver_id, :offer_name, :expires_at, :terms, :description)
    end

    def check_booking
      @booking=Booking.find_by( id: sub_vendor_params[:booking_id])
      if @booking
        if @booking[:dealership_id] != @sub_vendor[:dealership_id] 
          respond_to do |format|
            format.json { render json:{ code: 0, msg: "You can not access this booking." }}
          end        
        end 
      else
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Booking not found." }}
        end        
      end
    end

    def check_status
      if !Booking.statuses.include?(sub_vendor_params[:status])
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Invalid booking status." }}
        end
      end       
    end

    def check_driver
      if sub_vendor_params[:driver_id]!= "0"
        @driver=Driver.find_by(id: sub_vendor_params[:driver_id])
        if @driver
          if !@driver.deactivated
            if !@driver.vendor_id.nil?
              @temp_vendor=Vendor.find_by(id: @driver.vendor_id)
              #no need to check but still checking
              if @temp_vendor
                temp_dealerships=@temp_vendor.dealerships
                if !temp_dealerships.ids.include?(@sub_vendor[:dealership_id])
                  respond_to do |format|
                    format.json { render json:{ code: 0, msg: "This driver doesn't belong to this dealership." }}
                  end        
                end
              else
                respond_to do |format|
                  format.json { render json:{ code: 0, msg: "Admin not found." }}
                end            
              end
            elsif !@driver.sub_vendor_id.nil?
              @temp_sub_vendor=SubVendor.find_by(id: @driver.sub_vendor_id)
              if @temp_sub_vendor
                if @temp_sub_vendor[:dealership_id]!=@sub_vendor[:dealership_id]  
                  respond_to do |format|
                    format.json { render json:{ code: 0, msg: "Admin not found." }}
                  end        
                end          
              else
                respond_to do |format|
                  format.json { render json:{ code: 0, msg: "Admin not found." }}
                end 
              end      
            end
          else
            respond_to do |format|
              format.json { render json:{ code: 0, msg: "Driver not found." }}
            end            
          end  
        else
          respond_to do |format|
            format.json { render json:{ code: 0, msg: "Driver not found." }}
          end        
        end
      end  
    end

    def check_booking_compatibilty_with_driver
      if sub_vendor_params[:driver_id]!= "0"   
        if @driver.dealership_id != @booking.dealership_id
          respond_to do |format|
            format.json { render json:{ code: 0, msg: "This driver does not belong to this dealership." }}
          end 
        end 
      end
    end  

    def check_fields
      if sub_vendor_params[:offer_name] && sub_vendor_params[:terms] && sub_vendor_params[:description] && sub_vendor_params[:expires_at]
        if sub_vendor_params[:offer_name].empty? || sub_vendor_params[:terms].empty? || sub_vendor_params[:description].empty? || sub_vendor_params[:expires_at].empty?
          respond_to do |format|
            format.json { render json: {code: 0, msg: "one of the parameter is empty."}}
          end           
        end
      else
        respond_to do |format|
          format.json { render json: {code: 0, msg: "one of the parameter is not present."}}
        end        
      end
    end       
end
