class CreateWashPackages < ActiveRecord::Migration
  def change
    create_table :wash_packages do |t|
      t.string :model
      t.text :jobs
      t.integer :cost

      t.timestamps null: false
    end
  end
end
