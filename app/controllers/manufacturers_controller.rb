class ManufacturersController < ApplicationController
  before_action :set_manufacturer, only: [:show, :edit, :update, :destroy]

  # GET /manufacturers
  # GET /manufacturers.json
  def index
    @manufacturers = Manufacturer.order(:manufacturer)
    respond_to do |format|
      if !@manufacturers.empty?
        format.json { render json:{ code: 1, msg: "Listing all manufacturers.", manufacturers: @manufacturers.as_json(only: ['manufacturer', 'id', 'image'])}}
      else
        format.json { render json:{ code: 0, msg: "No manufacturers available." }}
      end
    end
  end

  # GET /manufacturers/1
  # GET /manufacturers/1.json
  def show
    respond_to do |format|
      format.json { render json:{ code: "1", msg: "Showing manufacturer.", manufactuer: @manufacturer }}
    end    
  end

  def all_vehicles
    @manufacturer = Manufacturer.find_by(id: manufacturer_params[:manufacturer_id])
    if @manufacturer.nil?
      respond_to do |format|
        format.json { render json:{ code: 0, msg: "Manufacturer not found." }}
      end
      return
    end     
    allvehicles=@manufacturer.vehiclemodels.order(:name)
    respond_to do |format|
      if !allvehicles.empty?
        format.json { render json:{ code: 1, msg: "Showing all vehicles of this manufacturer.", allvehicles: allvehicles.as_json(only: ['name','id']) }}
      else
        format.json { render json:{ code: 0, msg: "No vehicles present of this manufacturer." }}
      end
    end
  end

  # # GET /manufacturers/new
  # def new
  #   @manufacturer = Manufacturer.new
  # end

  # # GET /manufacturers/1/edit
  # def edit
  # end

  # POST /manufacturers
  # POST /manufacturers.json
  # def create
  #   @manufacturer = Manufacturer.new(manufacturer_params)

  #   respond_to do |format|
  #     if @manufacturer.save
  #       format.html { redirect_to @manufacturer, notice: 'Manufacturer was successfully created.' }
  #       format.json { render :show, status: :created, location: @manufacturer }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @manufacturer.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # PATCH/PUT /manufacturers/1
  # # PATCH/PUT /manufacturers/1.json
  # def update
  #   respond_to do |format|
  #     if @manufacturer.update(manufacturer_params)
  #       format.html { redirect_to @manufacturer, notice: 'Manufacturer was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @manufacturer }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @manufacturer.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # DELETE /manufacturers/1
  # # DELETE /manufacturers/1.json
  # def destroy
  #   @manufacturer.destroy
  #   respond_to do |format|
  #     format.html { redirect_to manufacturers_url, notice: 'Manufacturer was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_manufacturer
      @manufacturer = Manufacturer.find_by(id: params[:id])
      if @manufacturer.nil?
        respond_to do |format|
          format.json { render json:{ code: "0", msg: "Manufacturer not found." }}
        end
      end  
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def manufacturer_params
      params.permit(:manufacturer, :description, :manufacturer_id)
    end
end
