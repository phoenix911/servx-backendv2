class ChangeAddressFormatInUsers < ActiveRecord::Migration
  def change
  	change_column :users, :address, :text, default: "New Delhi"
  end
end
