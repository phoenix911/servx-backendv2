require 'test_helper'

class SubVendorsControllerTest < ActionController::TestCase
  setup do
    @sub_vendor = sub_vendors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sub_vendors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sub_vendor" do
    assert_difference('SubVendor.count') do
      post :create, sub_vendor: { dealership_id: @sub_vendor.dealership_id, email: @sub_vendor.email, mobile: @sub_vendor.mobile, name: @sub_vendor.name, password_digest: @sub_vendor.password_digest, remember_digest: @sub_vendor.remember_digest, vendor_id: @sub_vendor.vendor_id }
    end

    assert_redirected_to sub_vendor_path(assigns(:sub_vendor))
  end

  test "should show sub_vendor" do
    get :show, id: @sub_vendor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sub_vendor
    assert_response :success
  end

  test "should update sub_vendor" do
    patch :update, id: @sub_vendor, sub_vendor: { dealership_id: @sub_vendor.dealership_id, email: @sub_vendor.email, mobile: @sub_vendor.mobile, name: @sub_vendor.name, password_digest: @sub_vendor.password_digest, remember_digest: @sub_vendor.remember_digest, vendor_id: @sub_vendor.vendor_id }
    assert_redirected_to sub_vendor_path(assigns(:sub_vendor))
  end

  test "should destroy sub_vendor" do
    assert_difference('SubVendor.count', -1) do
      delete :destroy, id: @sub_vendor
    end

    assert_redirected_to sub_vendors_path
  end
end
