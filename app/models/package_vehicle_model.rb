class PackageVehicleModel < ActiveRecord::Base
  belongs_to :vehiclemodel
  belongs_to :package
  validates :package_id, presence: {message: "Package not found."}
  validates :vehiclemodel_id, presence: {message: "Model not found."}
end
