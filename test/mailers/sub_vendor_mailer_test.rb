require 'test_helper'

class SubVendorMailerTest < ActionMailer::TestCase
  test "offer_mail" do
    mail = SubVendorMailer.offer_mail
    assert_equal "Offer mail", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
