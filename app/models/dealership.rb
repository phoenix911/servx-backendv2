class Dealership < ActiveRecord::Base
	attr_accessor :type
	belongs_to :manufacturer
	has_many :bookings, dependent: :destroy
	has_many :getdealerships, dependent: :destroy
	has_many :reviews, dependent: :destroy
	has_many :vendors, through: :dealershipvendors
	has_many :dealershipvendors, dependent: :destroy
	has_many :locations, through: :getdealerships
	has_many  :sub_vendors, dependent: :destroy
	has_many  :service_advisors, dependent: :destroy
	has_many  :drivers, dependent: :destroy
	validates :name, presence: true
	validates :latitude, presence: true
	validates :longitude, presence: true
	#validates :email, presence: true  
	validates :manufacturer_id, presence: true 
	has_many :offers, through: :getoffers
	has_many :getoffers, dependent: :destroy	
	#mount_uploader :image, ImageUploader	
	validate  :image_size
	default_scope -> { order(:name) }
	attr_accessor :premium
	attr_accessor :rating

	def get_rating
		reviews=self.reviews
		totalRatings=self.reviews.count
		rating=0
		reviews.each do |review|
			rating+=review.rating
		end
		if totalRatings > 1 
			rating=rating/totalRatings.to_f
		end
		rating=rating.round(1)
		return rating
	end

	def self.all_ratings(dealers)
	  dealers.each do |dealer|
	    rating= dealer.get_rating
	    if rating==0.0
	      dealer.rating="N/A"
	    else
	      dealer.rating=rating
	    end
	  end
	end  

	def self.check_premium(dealers)
	  dealers.each do |dealer|
	    if dealer[:email]== "akansh@servx.in"
	      dealer.premium=false
	    else
	      dealer.premium=true
	    end
	  end
	end

  def set_relation(location_array)
    locations = eval(location_array)    
    locations.each do |location|
      Getdealership.create!(dealership_id: self.id, location_id: location)
    end    
  end

  private

     def image_size
      if image.size > 5.megabytes
        errors.add(:image, "Image size should be less than 5 MB")
      end
    end

end
