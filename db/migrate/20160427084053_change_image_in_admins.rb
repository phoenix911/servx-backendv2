class ChangeImageInAdmins < ActiveRecord::Migration
  def change
  	change_column :admins, :image, :string, default: "default.png"
  end
end
