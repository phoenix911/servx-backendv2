class Api::V1::UsersController < Api::V1::ApiController
  before_action :secure_user_api, only: [:user_vehicles, :user_notifications, :user_bookings, :user_addresses, :send_otp, :resend, :update, :create_address, :get_user_info, :remove_address]
  before_action :check_address,    only: [:create_address]
  
  def create
  	@user = User.new(user_params.except(:temp_otp, :address))
    if @user.save
      render json: {code:1,response:{ msg:"You will receive your OTP shortly."}}
    else
		  render json: {code:0,response:{ msg: @user.errors.messages}}
    end
  end 

  def send_otp
    @user.send_temp_otp
    render json: {code:1,response:{ msg: "You will receive your OTP shortly."}}
  end

  def resend
    @user.send_temp_otp
    render json:{code:1,response:{ msg:"Sorry for the delay. You will receive your OTP shortly."}}
  end

  def update
    if @user.temp_otp==user_params[:temp_otp].to_i
      if @user.update_attributes(user_params.except(:remember_digest, :temp_otp, :gcm_token, :apns_token))
        @user.update_multiple_values
        render json:{code: 1, response: { msg: "Profile successfully updated", user:@user.as_json(only: ['name','remember_digest','mobile','email'])}}
      else
        render json:{code: 0,response:{ msg:@user.errors.messages }}
      end
    else
        render json:{code: 0,response:{ msg:"OTP doesn't match."}}
    end
  end

  def user_vehicles
    vehicles=@user.vehicles.where(deactivated: false).order('id DESC')
    if !vehicles.empty?
      render json:{code: 1,response:{ msg: "Showing all your vehicles.", vehicles: vehicles.as_json(methods:[:image_and_model,:image,:model, :manufacturer_id],except: ['created_at','updated_at','user_id','manufacturer_id','deactivated']), vehicle_empty: 0 }}
    else
      render json:{ code: 0,response:{ msg: "No vehicles present.", vehicles: vehicles, vehicle_empty: 1 }}
    end
  end

  def user_bookings
      render json:{ code: 1,response: { msg: "Showing all your bookings.", user: @user.as_json( methods:[:all_bookings], only: ['email'])}} 
  end

  def user_notifications
    notifications=@user.notifications
    if !notifications.empty?
      render json:{code: 1,  response: {msg: "Showing all your notifications.", notifications: notifications.as_json(only: 'text')}}
    else
      render json:{code: 1, response:{ msg: "No notifications present.", notifications: notifications}}
    end
  end

  def user_addresses
    addresses=@user.addresses
    if !addresses.empty?
      render json:{code: 1,  response: {msg: "Showing all your addresses.", addresses: addresses.as_json(only: [ 'text', 'label'])}}
    else
      render json:{code: 1, response:{ msg: "No addresses present.", addresses: addresses}}
    end
  end  

  def remove_address
    addresses=@user.addresses.where(label: user_params[:label])
    if !addresses.empty?
      addresses.first.destroy
      render json:{ code:1,response:{msg: "Address removed." }}
    else
      render json:{ code:1,response:{msg: "Address not found." }}
    end
  end  

  def create_address
    @address = @user.addresses.build(user_params.slice(:text, :label))
    if @address.save
      render json: {code:1,response:{ msg:"Created address successfully."}}
    else
      render json: {code:0,response:{ msg: @address.errors.messages}}
    end
  end

  def get_user_info
    render json: {code: 1, response: { user: @user.as_json(include: {active_vehicles: {methods:[:image_and_model,:image,:model],only: ['id', 'reg_num', 'vehiclemodel_id']}, addresses:{only: ['text', 'label']}},only: ['name','id','remember_digest','mobile','email'])}}
  end

private
    # Use callbacks to share common setup or constraints between actions.
    def user_params
      params.permit(:name, :email, :password, :address, :mobile, :temp_otp, :gcm_token, :apns_token, :remember_digest, :label, :text)
    end	

    def check_address
      addresses=@user.addresses.where(label: user_params[:label])
      if !addresses.empty?
        render json:{ code:1,response:{msg: "Address with this label already exist." }}
      end
    end
end

