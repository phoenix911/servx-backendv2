class Getdealership < ActiveRecord::Base
  belongs_to :dealership
  belongs_to :location
  validates :dealership_id, presence: true
  validates :location_id, presence: true
end
