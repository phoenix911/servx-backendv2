json.array!(@offers) do |offer|
  json.extract! offer, :id, :name, :image, :expires_at
  json.url offer_url(offer, format: :json)
end
