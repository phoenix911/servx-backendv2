class AddLastActiveToSubVendor < ActiveRecord::Migration
  def change
    add_column :sub_vendors, :last_active, :datetime
  end
end
