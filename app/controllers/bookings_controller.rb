class BookingsController < ApplicationController
	before_action :set_booking,          					only: [ :update, :destroy]
	before_action :check_booking,        					only: [:update, :destroy]
	before_action :logged_in_user,        				only: [:create, :update, :destroy]
	before_action :check_user_and_vehicle,				only: [:create]  
	before_action :check_time,										only: [:create],if: :apply_offer?	
	before_action :check_offer,										only: [:create],if: :apply_offer?
	before_action :check_applies_on_dealership,	only: [:create],if: :apply_offer?
	#
	#
	#
	#
	#############################################################################
	## WHEN USER TRIES TO UPDATE A BOOKING CHECK VEHICLE'S DEACTIVATED BOOLEAN###
	#############################################################################
	#
	#
	#
	#
	#
	#
	#
	##

	# GET /bookings
	# GET /bookings.json
	#accesed by admin
	# def index
	#   @bookings = Booking.all
	#   respond_to do |format|
	#     format.json { render json:{ code: "1", msg: "Showing all bookings.", bookings: @bookings }}
	#   end
	# end

	# GET /bookings/1
	# GET /bookings/1.json
	# def show
	#   respond_to do |format|
	#     format.json { render json:{ code: "1", msg: "Showing booking.", booking: @booking}}
	#   end
	# end


	# POST /bookings
	# POST /bookings.json
	def create
		@booking = Booking.new(booking_params.except(:remember_digest, :feedback, :booking_id, :address, :apply, :offer))
		if apply_offer?
			@booking.offer_id=@offer.id
		end
		respond_to do |format|
			if !@user.check_address(booking_params[:address])
				format.json { render json:{ code: 0, msg: @user.errors, hash: 1 }}
			elsif @booking.save
				@booking.generate_reference_code
				@user.last_active_update
				dealership= Dealership.includes(:manufacturer).where(id: @booking[:dealership_id])
				manufacturer=dealership[0].manufacturer
				@booking.send_user_booking_mail(@user,@vehicle,dealership[0],manufacturer)
				@booking.send_admin_booking_mail(@user,@vehicle,dealership[0])
				# not verifying dealership. Takes an extra query.
				#send a mail to dealership too. 
				# messages too.       
				if apply_offer?
				format.json { render json:{ code: 1, msg: "Booking successfully created. Our executives will contact you shortly.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time']), user: @user.as_json(only: 'address'), hash: 0, offer: "Offer used: #{@offer.name}" }}					
				else
				format.json { render json:{ code: 1, msg: "Booking successfully created. Our executives will contact you shortly.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time']), user: @user.as_json(only: 'address'), hash: 0 }}
				end
			else
				format.json { render json:{ code: 0, msg: @booking.errors.messages, hash: 1  }}
			end
		end
	end

	# PATCH/PUT /bookings/1
	# PATCH/PUT /bookings/1.json
	def update
		respond_to do |format|
			if !@user.check_address(booking_params[:address])
				format.json { render json:{ code: "0", msg: @user.errors }}
			end       
			if @booking.update_attributes(booking_params.except(:remember_digest,:booking_id, :user_id, :vehicle_id, :dealership_id, :address))
				format.json { render json:{ code: "1", msg: "Booking successfully updated.", booking: @booking}}
			else
				format.json { render json:{ code: "0", msg: @booking.errors.messages }}
			end
		end
	end

	# DELETE /bookings/1
	# DELETE /bookings/1.json
	def destroy
		@booking.destroy
		respond_to do |format|
			format.json { render json:{ code: 1, msg: "Booking successfully removed." }}
		end
	end

	def booking_statuses
		respond_to do |format|
			format.json { render json:{ code: 1, msg: "All booking statuses.", status: Booking.statuses }}
		end		
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_booking
			@booking = Booking.find_by(id: params[:booking_id])
			if @booking.nil? 
				respond_to do |format|
					format.json { render json:{ code: 0, msg: "Booking not found." }}
				end
			end
		end

		def check_booking
			if @booking.user_id!=params[:user_id].to_i
				respond_to do |format|         
					format.json { render json:{ code: 0, msg: "Invalid access to this booking." }}
				end        
			end
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def booking_params
			params.permit(:user_id, :vehicle_id, :dealership_id, :pickup_time, :urgent, :feedback, :jobs, :remember_digest, :booking_id, :address, :offer, :apply)
		end

		def check_user_and_vehicle
			@vehicle=Vehicle.find_by(id: booking_params[:vehicle_id])
			if @vehicle.nil? || @vehicle.deactivated==true
				respond_to do |format|           
					format.json { render json:{ code: 0, msg: "Vehicle not found.", hash: 0 }} 
				end       
			elsif @vehicle.user_id!=booking_params[:user_id].to_i
				respond_to do |format|         
					format.json { render json:{ code: 0, msg: "Invalid access. You can not make a booking on this vehicle.", hash: 0 }}
				end
			end  
		end

		def check_time
			if !booking_params[:offer].nil?
				@offer= Offer.find_by(name: booking_params[:offer].upcase)
				if @offer.nil?
					respond_to do |format|
						format.json { render json:{ code: 0, msg: "Invalid offer.", hash: 0 }}
					end				
				elsif !@offer.expires_at.nil?
					if @offer.expires_at < Time.zone.now
						respond_to do |format|
							format.json { render json:{ code: 0, msg: "This offer has expired.", hash: 0 }}
						end
					end
				end
			else
				respond_to do |format|
					format.json { render json:{ code: 0, msg: "Please apply an offer.", hash: 0 }}
				end	
			end	
		end		

		def check_offer			
			jobs=eval(booking_params[:jobs])				
			if jobs.size==1 && jobs.first == "Car wash/ Dry cleaning"
				if @offer.wash == false
					respond_to do |format|
						format.json { render json:{ code: 0, msg: "Sorry. This offer is not applicable on car wash/ Dry cleaning.", hash: 0 }}
					end					
				end
			elsif !jobs.include?("Car wash/ Dry cleaning")
				if @offer.wash == true
					respond_to do |format|				
						format.json { render json:{ code: 0, msg: "Sorry. This offer is not applicable without car wash/ Dry cleaning.", hash: 0 }}
					end	
				end
			end			
		end

		def check_applies_on_dealership
			if !@offer.all
				dealerships= @offer.dealerships
				if !dealerships.ids.include?(booking_params[:dealership_id])
					respond_to do |format|				
						format.json { render json:{ code: 0, msg: "Sorry. This offer is not applicable on this manufacturer.", hash: 0 }}
					end
				end	
			end	
		end	

		def apply_offer?
			if booking_params[:apply]== "false" || booking_params[:apply]== "0" || booking_params[:apply]== 0
				return false
			elsif booking_params[:apply]== "true" || booking_params[:apply]== "1" || booking_params[:apply]== 1
				return true 
			end
		end
end
