class AddDeactivatedToVehicles < ActiveRecord::Migration
  def change
    add_column :vehicles, :deactivated, :boolean, default: false
  end
end
