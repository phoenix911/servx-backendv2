require 'test_helper'

class PackageVehicleModelsControllerTest < ActionController::TestCase
  setup do
    @package_vehicle_model = package_vehicle_models(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:package_vehicle_models)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create package_vehicle_model" do
    assert_difference('PackageVehicleModel.count') do
      post :create, package_vehicle_model: { package_id: @package_vehicle_model.package_id, vehiclemodel_id: @package_vehicle_model.vehiclemodel_id }
    end

    assert_redirected_to package_vehicle_model_path(assigns(:package_vehicle_model))
  end

  test "should show package_vehicle_model" do
    get :show, id: @package_vehicle_model
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @package_vehicle_model
    assert_response :success
  end

  test "should update package_vehicle_model" do
    patch :update, id: @package_vehicle_model, package_vehicle_model: { package_id: @package_vehicle_model.package_id, vehiclemodel_id: @package_vehicle_model.vehiclemodel_id }
    assert_redirected_to package_vehicle_model_path(assigns(:package_vehicle_model))
  end

  test "should destroy package_vehicle_model" do
    assert_difference('PackageVehicleModel.count', -1) do
      delete :destroy, id: @package_vehicle_model
    end

    assert_redirected_to package_vehicle_models_path
  end
end
