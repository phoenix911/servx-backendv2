class DriversController < ApplicationController
 before_action :logged_in_driver, only: [:signout, :bookings, :booking, :update_booking]
 before_action :check_booking,    only: [:booking, :update_booking]
 before_action :check_status,     only: [:update_booking] 

  # GET /drivers
  # GET /drivers.json


  def signin
    @driver= Driver.find_by(mobile: driver_params[:mobile])
    respond_to do |format|
      if @driver && @driver.authenticate(driver_params[:password])
        @driver.update_remember
        format.json { render json: {code: 1, msg: "Successfully logged in!", driver: @driver.as_json(only: ['name','id','remember_digest','mobile','email', 'image'])}}
      elsif @driver
        format.json { render json: {code: 0, msg: "Invalid password"}}
      else
        format.json { render json: {code: 0, msg: "Invalid email/password combination."}}
      end
    end  
  end

  def signout
    @driver.forget
    respond_to do |format|
      format.json { render json: {code: 1, msg: "Successfully logged out."}}       
    end
  end  

  def bookings
    bookings=Booking.where('pickup_driver = ? OR drop_driver = ?', @driver[:id], @driver[:id]).order('id DESC')
    respond_to do |format|
      format.json { render json:{ code: 1, bookings: bookings.as_json(include: {user: {only: ['name','mobile',"address"]},
        dealership: {only: ['id','name','address','image']},vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},
        only: ['reference_code','jobs','pickup_time','drop_time', 'status', 'cost', 'id'])}}       
    end    
  end

  def booking
    @booking.change_job_format
    respond_to do |format|
        pickup_driver=Driver.find_by(id: @booking[:pickup_driver])
        drop_driver=Driver.find_by(id: @booking[:drop_driver])
        if pickup_driver && drop_driver
        format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
            vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
            pickup_driver: pickup_driver.as_json(only: ['id', 'name', 'mobile', 'image']), drop_driver: drop_driver.as_json(only: ['id', 'name', 'mobile', 'image'])}}
        elsif pickup_driver
          format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
            vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
            pickup_driver: pickup_driver.as_json(only: ['id', 'name', 'mobile', 'image'])}}
        elsif drop_driver
          format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
            vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
            drop_driver: drop_driver.as_json(only: ['id', 'name', 'mobile', 'image'])}} 
        else
          format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
            vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}}, only: ['reference_code','jobs','pickup_time', 'status', 'cost'])}} 
        end           
    end   
  end

  def update_booking
    respond_to do |format|
     driver_params[:status]= Booking.statuses[driver_params[:status]]
      if @booking.update_columns(driver_params.slice(:status, :pickup_time, :jobs, :cost))
        format.json { render json:{ code: 1, msg: "updated booking.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}               
      else
        format.json { render json:{ code: 0, msg: @booking.errors.messages }}
      end  
    end     
  end
  # # GET /drivers/1
  # # GET /drivers/1.json
  # def show
  # end

  # # GET /drivers/new
  # def new
  #   @driver = Driver.new
  # end

  # # GET /drivers/1/edit
  # def edit
  # end

  # # POST /drivers
  # # POST /drivers.json
  # def create
  #   @driver = Driver.new(driver_params)

  #   respond_to do |format|
  #     if @driver.save
  #       format.html { redirect_to @driver, notice: 'Driver was successfully created.' }
  #       format.json { render :show, status: :created, location: @driver }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @driver.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /drivers/1
  # PATCH/PUT /drivers/1.json
  # def update
  #   respond_to do |format|
  #     if @driver.update(driver_params)
  #       format.html { redirect_to @driver, notice: 'Driver was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @driver }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @driver.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /drivers/1
  # DELETE /drivers/1.json
  # def destroy
  #   @driver.destroy
  #   respond_to do |format|
  #     format.html { redirect_to drivers_url, notice: 'Driver was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_driver
      @driver = Driver.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def driver_params
      @driver_params ||= params.permit(:name, :email, :password, :remember_digest, :mobile, :booking_id, :jobs, :cost, :pickup_time, :status)
    end
    
    def check_booking
      @booking=Booking.find_by( id: driver_params[:booking_id])
      if @booking
        if @booking[:pickup_driver] != @driver[:id] && @booking[:drop_driver] != @driver[:id] 
          respond_to do |format|
            format.json { render json:{ code: 0, msg: "You can not access this booking." }}
          end        
        end 
      else
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Booking not found." }}
        end        
      end
    end

    def check_status
      if !Booking.statuses.include?(driver_params[:status])
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Invalid booking status." }}
        end
      end       
    end    
end
