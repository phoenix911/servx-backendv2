class RemoveSlotFromBookingsAndWashBookings < ActiveRecord::Migration
  def change
  	change_column :bookings, :slot, :string
  	change_column :wash_bookings, :slot, :string
  end
end
