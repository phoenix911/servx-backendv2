class AdminsController < ApplicationController
  before_action :logged_in_admin, only: [:dealerships, :users, :bookings, :manufacturers, :create_vendor, :user_details, :booking, :get_vendors, :get_sub_vendors,
                                         :get_drivers, :create_dealership, :update_booking,:create_offer,:remove_offer]
  before_action :check_booking,   only: [:booking, :update_booking]  
  before_action :check_user,      only: [:user_details]    
  before_action :check_array_param,   only: [:create_vendor, :create_dealership] 
  before_action :check_driver,    only: [:driver_details]      
  before_action :check_status,     only: [:update_booking] 
  before_action :check_offer_on,     only: [:create_offer] 

  def signin
    @admin= Admin.find_by(mobile: admin_params[:mobile])
    respond_to do |format|
      if @admin && @admin.authenticate(admin_params[:password])
        @admin.update_remember
        format.json { render json: {code: 1, msg: "Successfully logged in!", admin: @admin.as_json(only: ['name','id','remember_digest','mobile','email','image'])}}
      elsif @admin
        format.json { render json: {code: 0, msg: "Invalid password"}}
      else
        format.json { render json: {code: 0, msg: "Invalid mobile/password combination."}}
      end
    end
  end

  def bookings
    respond_to do |format|
      if admin_params[:dealership_id] && !admin_params[:dealership_id].empty?
        bookings = Booking.where(dealership_id: admin_params[:dealership_id].to_i).order('id DESC')
        if bookings.empty?
          format.json { render json:{ code: 0, msg: "No bookings found.", bookings: bookings}}
        else
          format.json { render json:{ code: 1, msg: "Listing all bookings of given dealership.", bookings: bookings.as_json(include: {user: {only: ['id', 'name','mobile','address']},
            vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}}, only: ['id', 'reference_code','jobs','pickup_time', 'status', 'drop_time']) }}
        end         
      else
        bookings = Booking.all.order('id DESC')
        if bookings.empty?
          format.json { render json:{ code: 0, msg: "No bookings found.", bookings: bookings}}
        else
          format.json { render json:{ code: 1, msg: "Listing all bookings.", bookings: bookings.as_json(include: {user: {only: ['id', 'name','mobile','address']},
            vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}}, only: ['id', 'reference_code','jobs','pickup_time', 'status', 'drop_time']) }}
        end 
      end 
    end  
  end

  def users
    respond_to do |format|
      users = User.all.order('id DESC')
      if users.nil?
        format.json { render json:{ code: 0, msg: "No users found.", users: users}}
      else
        format.json { render json:{ code: 1, msg: "Listing all users.", users: users.as_json(only: ['id', 'name', 'mobile', 'email']) }}
      end 
    end  
  end  

  def user_details
    respond_to do |format|
      format.json { render json:{ code: 1, msg: "Listing all details of #{@user.name}.",
        user: @user.as_json(include: {vehicles: {include: {bookings: {only: ['reference_code','jobs','pickup_time', 'status', 'cost']}},
        only: ['model','image','registration_number']}},only: ['name','mobile','email', 'last_active']) }}
    end  
  end    

  def manufacturers
    respond_to do |format|
      manufacturers = Manufacturer.all
      if manufacturers.nil?
        format.json { render json:{ code: 0, msg: "No manufacturers found.", manufacturers: manufacturers}}
      else
        format.json { render json:{ code: 1, msg: "Listing all manufacturers.", manufacturers:manufacturers.as_json(include: {dealerships: {only: ['id', 'name', 'id', 'address', 'image']}},
                                                                                                                                                   only: ['id', 'manufacturer', 'image']) }}
      end 
    end  
  end

  def dealerships
    respond_to do |format|
      dealerships = Dealership.all
      if dealerships.nil?
        format.json { render json:{ code: 0, msg: "No dealerships found.", dealerships: dealerships}}
      else
        format.json { render json:{ code: 1, msg: "Listing all dealerships.", dealerships: dealerships.as_json( only: ['name', 'id', 'address', 'image']) }}
      end 
    end  
  end

  def create_vendor
    @vendor = Vendor.new(admin_params.slice(:mobile, :email, :name))
    password=@vendor.set_password
    respond_to do |format|
      if @vendor.save
        @vendor.set_relation(admin_params[:array])
        @admin.send_mail(@vendor,password)
        format.json { render json:{code: 1, msg: "Vendor created!" }}
      else
        format.json { render json:{code: 0, msg: @vendor.errors.messages}}
      end
    end
  end

  def create_dealership
    if admin_params[:image]
      tempfile = Tempfile.new("fileupload")
      tempfile.binmode
      tempfile.write(Base64.decode64(admin_params[:image]))
      uploaded_file = ActionDispatch::Http::UploadedFile.new(tempfile: tempfile, filename: admin_params[:name], original_filename: admin_params[:name]) 
      admin_params[:image] =  uploaded_file
      tempfile.delete
    end    
    @dealership = Dealership.new(admin_params.slice(:name, :address, :image, :gm_name, :gm_email, :gm_phone, :wm_name, :wm_email,
                                                    :wm_phone, :ccm_name, :ccm_email, :ccm_phone, :latitude, :longitude, :manufacturer_id, :email, :image))
     @dealership.type=admin_params[:image_type]
    respond_to do |format|
      if @dealership.save
        @dealership.set_relation(admin_params[:array])
        format.json { render json:{code: 1, msg: "Dealership created!" }}
      else
        format.json { render json:{code: 0, msg: @dealership.errors.messages}}
      end
    end
  end

  def booking
    @booking.change_job_format
    respond_to do |format|
      pickup_driver=Driver.find_by(id: @booking[:pickup_driver])
      drop_driver=Driver.find_by(id: @booking[:drop_driver])
      if pickup_driver && drop_driver
        format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
        vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
        pickup_driver: pickup_driver.as_json(only: ['id', 'name', 'mobile', 'image', 'available']), drop_driver: drop_driver.as_json(only: ['id', 'name', 'mobile', 'image','available'])}}
      elsif pickup_driver
        format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
        vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
        pickup_driver: pickup_driver.as_json(only: ['id', 'name', 'mobile', 'image','available'])}}
      elsif drop_driver
        format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
        vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
        drop_driver: drop_driver.as_json(only: ['id', 'name', 'mobile', 'image','available'])}} 
      else
        format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
        vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}}, only: ['reference_code','jobs','pickup_time', 'status', 'cost'])}} 
      end                   
    end  
  end

  def update_booking
    respond_to do |format|
     admin_params[:status]= Booking.statuses[admin_params[:status]]
      if @booking.update_columns(admin_params.slice(:status, :pickup_time, :jobs, :cost))
        if admin_params[:status]=="Your vehicle has reached the service centre"
          if !@booking.pickup_driver.nil?
            driver=Driver.find_by(@booking.pickup_driver)
            if driver
              driver.update_status(1)
            end
          end 
          if driver
            format.json { render json:{ code: 1, msg: "updated booking. Updated #{driver.name}'s status with available", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}               
          else
            format.json { render json:{ code: 1, msg: "updated booking.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}               
          end
        elsif admin_params[:status]=="Payment received"
          if !@booking.drop_driver.nil?
            driver=Driver.find_by(@booking.drop_driver)
            if driver
              driver.update_status(1)
            end
          end 
          if driver
            format.json { render json:{ code: 1, msg: "updated booking status. Updated #{driver.name}'s status with available", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}               
          else
            format.json { render json:{ code: 1, msg: "updated booking.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}                         
          end
        else
          format.json { render json:{ code: 1, msg: "updated booking.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}  
        end
      else
        format.json { render json:{ code: 0, msg: @booking.errors.messages }}
      end  
    end 
  end  

  def get_vendors
    respond_to do |format|
      if admin_params[:dealership_id] && !admin_params[:dealership_id].empty?
        dealership= Dealership.find_by(id: admin_params[:dealership_id])
        if dealership
          vendors= dealership.vendors
          if vendors.empty?
            format.json { render json:{ code: 0, msg: "No vendors found.", vendors: vendors}} 
          else
            format.json { render json:{ code: 1, msg: "Listing all vendors of this dealership.", vendors: vendors.as_json(include: {dealerships: {only: ['name', 'address', 'image']}},
                                                                                                                            only: ['name', 'email', 'mobile', 'last_active']) }}
          end
        else
          format.json { render json:{code: 1, msg: "Dealership not found" }}          
        end        
      else      
        vendors = Vendor.all.order('id DESC')
        if vendors.nil?
          format.json { render json:{ code: 0, msg: "No vendors found.", vendors: vendors}}
        else
          format.json { render json:{ code: 1, msg: "Listing all vendors.", vendors: vendors.as_json(include: {dealerships: {only: ['name', 'address', 'image']}},
                                                                                                                            only: ['name', 'email', 'mobile', 'last_active']) }}
        end
      end  
    end  
  end  

  def get_sub_vendors
    respond_to do |format|
      if admin_params[:dealership_id] && !admin_params[:dealership_id].empty?
        sub_vendors = SubVendor.where(dealership_id: admin_params[:dealership_id].to_i).order('id DESC')
        if sub_vendors.empty?
          format.json { render json:{ code: 0, msg: "No sub vendors found.", subvendors: sub_vendors}}
        else
          format.json { render json:{ code: 1, msg: "Listing all sub vendors of given dealership.", subvendors: sub_vendors.as_json(only: ['name', 'email', 'mobile', 'last_active']) }}
        end         
      else
        sub_vendors = SubVendor.all.order('id DESC')
        if sub_vendors.empty?
          format.json { render json:{ code: 0, msg: "No bookings found.", subvendors: sub_vendors}}
        else
          format.json { render json:{ code: 1, msg: "Listing all sub vendors.", subvendors: sub_vendors.as_json( only:  ['name', 'email', 'mobile', 'last_active']) }}
        end 
      end 
    end  
  end

  def get_drivers
    respond_to do |format|
      if admin_params[:dealership_id] && !admin_params[:dealership_id].empty?
        @drivers= Driver.where(dealership_id: admin_params[:dealership_id].to_i).order('id DESC')
        if @drivers.empty?
          format.json { render json:{ code: 0, msg: "No drivers found.", drivers: @drivers}}
        else
          format.json { render json:{ code: 1, msg: "Listing all drivers of given dealership.", drivers: @drivers.as_json(methods: :bookings_count, only: ['id', 'name', 'email', 'mobile', 'available']) }}
        end         
      else
        @drivers = Driver.all.order('id DESC')
        if @drivers.empty?
          format.json { render json:{ code: 0, msg: "No drivers found.", drivers: @drivers}}
        else
          format.json { render json:{ code: 1, msg: "Listing all drivers.", drivers: @drivers.as_json(methods: :bookings_count, only:  ['id', 'name', 'email', 'mobile', 'available']) }}
        end 
      end 
    end  
  end

  def driver_details
    bookings=Booking.where('pickup_driver = ? OR drop_driver = ?', admin_params[:driver_id], admin_params[:driver_id])
    respond_to do |format|
      if !bookings.empty?
        format.json { render json: {code: 1, msg: "Showing all bookings assigned to this driver.", bookings: bookings.as_json(only: ['id', 'reference_code','jobs','pickup_time','status', 'cost']),
         driver: @driver.as_json(only: ['name', 'email', 'mobile', 'available']) }}      
      else
        format.json { render json: {code: 1, msg: "No bookings found."}} 
      end
    end  
  end  

  def create_offer
    if admin_params[:image]
      tempfile = Tempfile.new("fileupload")
      tempfile.binmode
      tempfile.write(Base64.decode64(admin_params[:image]))
      uploaded_file = ActionDispatch::Http::UploadedFile.new(tempfile: tempfile, filename: admin_params[:name], original_filename: admin_params[:name]) 
      admin_params[:image] =  uploaded_file
      tempfile.delete
    end     
    @offer = Offer.new(admin_params.slice(:name,:terms,:description,:expires_at,:all,:wash,:image))
    @offer.type=admin_params[:image_type]
    respond_to do |format|
      if @offer.save
        if admin_params[:all].nil?
          if admin_params[:dealership_ids]
            @offer.add_dealerships(admin_params[:dealership_ids])
          elsif admin_params[:manufacturer_ids]
            @offer.add_manufacturers(admin_params[:manufacturer_ids])
          end
        end
        format.json { render json:{code: 1, msg: "Offer created!"}}
      else
        format.json { render json:{code: 0, msg: @offer.errors.messages}}
      end
    end    
  end

  def remove_offer
    offer = Offer.find_by(id: params[:offer_id])
    respond_to do |format|
      if offer
        name = offer.name
          offer.deactivate
        format.json { render json:{ code: 1, msg: "deactivated #{name}" }}  
      else
        format.json { render json:{ code: 0, msg: "Offer does not exist" }}  
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_params     
     @admin_params ||= params.permit(:name, :mobile, :email, :password, :image, :remeber_digest, :dealership_id, :array, :booking_id, :user_id, :driver_id, :admin_id, :image_type,
      :address, :image, :gm_name, :gm_email, :gm_phone, :wm_name, :wm_email,:wm_phone, :ccm_name, :ccm_email, :ccm_phone, :latitude, :longitude, :offer_id, :remember_digest,
      :manufacturer_ids, :status, :pickup_time, :jobs, :cost, :image, :name, :expires_at, :terms, :description, :all, :dealership_ids, :wash, :array, :manufacturer_id, :image)
    end

    def check_booking
      @booking = Booking.find_by(id: admin_params[:booking_id])
      if @booking.nil? 
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Booking not found." }}
        end
      end
    end
    
    def check_driver_param
      if admin_params[:driver_id].nil? || admin_params[:driver_id].empty?
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Driver not found." }}
        end
      end
    end

    def check_user
      @user = User.find_by(id: admin_params[:user_id])
      if @user.nil? 
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "user not found." }}
        end
      end
    end

    def check_driver
      @driver = Driver.find_by(id: admin_params[:driver_id])
      if @driver.nil? 
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "driver not found." }}
        end
      end
    end        

    def check_array_param
      if !admin_params[:array] || admin_params[:array].empty?
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Required array for defining relationship is not present." }}
        end
      end
    end

    def check_status
      if !Booking.statuses.include?(admin_params[:status])
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Invalid booking status." }}
        end
      end       
    end   

    def check_offer_on
      if admin_params[:all] && admin_params[:dealership_ids] && admin_params[:manufacturer_ids]
          respond_to do |format|
            format.json { render json: {code: 0, msg: "Are you crazy?!?!"}}
          end 
      elsif admin_params[:all].nil? && admin_params[:dealership_ids].nil? && admin_params[:manufacturer_ids].nil?
          respond_to do |format|
            format.json { render json: {code: 0, msg: "Add atleast one option from three given."}}
          end 
      elsif admin_params[:all]
        if admin_params[:all].empty?
          respond_to do |format|
            format.json { render json: {code: 0, msg: "Put value in parameter."}}
          end  
        end               
      elsif admin_params[:all].nil?
        if admin_params[:dealership_ids] && admin_params[:manufacturer_ids] 
          respond_to do |format|
            format.json { render json: {code: 0, msg: "Add a manufacturer or a dealership."}}
          end           
        elsif admin_params[:dealership_ids]
          if admin_params[:dealership_ids].empty?
            respond_to do |format|
              format.json { render json: {code: 0, msg: "Add dealerships."}}
            end  
          end           
        elsif admin_params[:manufacturer_ids]
          if admin_params[:manufacturer_ids].empty?
            respond_to do |format|
              format.json { render json: {code: 0, msg: "Add manufacturer."}}
            end 
          end
        else
          respond_to do |format|
            format.json { render json: {code: 0, msg: "No manufacturer or dealerships added."}}
          end 
        end
      end 
    end

end
