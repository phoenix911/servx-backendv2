class AdminMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.admin_mailer.vendor_info.subject
  #
  def vendor_info(vendor,password)
    @vendor		= vendor
    @password =	password

    require 'mailgun'
    # PUT API-KEY IN ENVIRONMENT VARIABLE FILE!!!keeping it open for development right now... but ServX's repository is private...so...
    # First, instantiate the Mailgun Client with your API key
    mg_client = Mailgun::Client.new "key-792d235a48f4513679ff561935d44bb5"

   html = render_to_string template: "admin_mailer/vendor_info.html.erb"
    text= render_to_string template: "admin_mailer/vendor_info.text.erb"
    # Define your message parameters
    message_params = {:from    => 'ServX <contact@servx.in>',  
                      :to      => @vendor.email,
                      :subject => 'vendor login details',
                      :text    => text.to_str,
                      :html    => html.to_str}

    # Send your message through the client
    mg_client.send_message "connx.in", message_params  
  end

  def form(name,mobile,msg)
    @name   = name
    @mobile = mobile
    @msg= msg

    require 'mailgun'
    # PUT API-KEY IN ENVIRONMENT VARIABLE FILE!!!keeping it open for development right now... but ServX's repository is private...so...
    # First, instantiate the Mailgun Client with your API key
    mg_client = Mailgun::Client.new "key-792d235a48f4513679ff561935d44bb5"

   html = render_to_string template: "admin_mailer/form.html.erb"
    # Define your message parameters
    message_params = {:from    => 'ServX <contact@servx.in>',  
                      :to      => "akansh@servx.in",
                      :subject => 'Form',
                      :html    => html.to_str}

    # Send your message through the client
    mg_client.send_message "connx.in", message_params  
  end  

end
