class ChangeUrgentFormatInBookings < ActiveRecord::Migration
  def change
  	change_column :bookings, :urgent, :boolean, default: false
  end
end
