require 'test_helper'

class JobPackagesControllerTest < ActionController::TestCase
  setup do
    @job_package = job_packages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:job_packages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create job_package" do
    assert_difference('JobPackage.count') do
      post :create, job_package: { job_id: @job_package.job_id, package_id: @job_package.package_id, quantity: @job_package.quantity }
    end

    assert_redirected_to job_package_path(assigns(:job_package))
  end

  test "should show job_package" do
    get :show, id: @job_package
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @job_package
    assert_response :success
  end

  test "should update job_package" do
    patch :update, id: @job_package, job_package: { job_id: @job_package.job_id, package_id: @job_package.package_id, quantity: @job_package.quantity }
    assert_redirected_to job_package_path(assigns(:job_package))
  end

  test "should destroy job_package" do
    assert_difference('JobPackage.count', -1) do
      delete :destroy, id: @job_package
    end

    assert_redirected_to job_packages_path
  end
end
