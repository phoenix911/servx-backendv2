class AddUniqueConstraintsToAdmins < ActiveRecord::Migration
  def change
  	add_index :admins, :mobile, unique: true
  	add_index :admins, :email, unique: true  	  	
  end
end
