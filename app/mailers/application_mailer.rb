class ApplicationMailer < ActionMailer::Base
  default from: "contact@servx.in"
  layout 'mailer'
end
