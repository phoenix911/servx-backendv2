class CreateAdmins < ActiveRecord::Migration
  def change
    create_table :admins do |t|
      t.string :name
      t.string :mobile
      t.string :email
      t.string :password_digest
      t.string :image
      t.string :remember_digest

      t.timestamps null: false
    end
  end
end
