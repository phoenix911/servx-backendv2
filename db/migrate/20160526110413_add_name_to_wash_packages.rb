class AddNameToWashPackages < ActiveRecord::Migration
  def change
    add_column :wash_packages, :name, :string
  end
end
