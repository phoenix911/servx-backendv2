class CreateServiceAdvisors < ActiveRecord::Migration
  def change
    create_table :service_advisors do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.string :remember_digest
      t.string :mobile
      t.boolean :deactivated
      t.references :vendor, index: true, foreign_key: true
      t.references :sub_vendor, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
