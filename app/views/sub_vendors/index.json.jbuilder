json.array!(@sub_vendors) do |sub_vendor|
  json.extract! sub_vendor, :id, :name, :email, :password_digest, :remember_digest, :mobile, :dealership_id, :vendor_id
  json.url sub_vendor_url(sub_vendor, format: :json)
end
