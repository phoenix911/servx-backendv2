class Getoffer < ActiveRecord::Base
  belongs_to :dealership
  belongs_to :offer
  validates :dealership_id, presence: true
  validates :offer_id, presence: true  
end
