class CreateDealershipvendors < ActiveRecord::Migration
  def change
    create_table :dealershipvendors do |t|
      t.references :vendor, index: true, foreign_key: true
      t.references :dealership, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :dealershipvendors, [:vendor_id, :dealership_id], unique: true
  end
end
