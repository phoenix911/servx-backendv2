json.array!(@package_vehicle_models) do |package_vehicle_model|
  json.extract! package_vehicle_model, :id, :vehiclemodel_id, :package_id
  json.url package_vehicle_model_url(package_vehicle_model, format: :json)
end
