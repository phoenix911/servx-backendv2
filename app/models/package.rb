class Package < ActiveRecord::Base
	has_many :vehiclemodels, through: :package_vehicle_models
	has_many :package_vehicle_models, dependent: :destroy
	has_many :bookings
	has_many :jobs, through: :job_packages
	has_many :job_packages, dependent: :destroy
	default_scope -> { order(cost: :asc) }
end
