class Vehiclemodel < ActiveRecord::Base
  belongs_to :manufacturer
	has_many :jobs, through: :prices
	has_many :prices, dependent: :destroy
	has_many :vehicles, dependent: :destroy
	default_scope -> { order(:name) }
	has_many :packages, through: :package_vehicle_models
	has_many :package_vehicle_models, dependent: :destroy
end
