class AddDropDriverToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :drop_driver, :integer
  end
end
