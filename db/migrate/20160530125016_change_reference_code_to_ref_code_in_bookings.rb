class ChangeReferenceCodeToRefCodeInBookings < ActiveRecord::Migration
  def change
  	rename_column :bookings, :reference_code, :ref_code
  end
end
