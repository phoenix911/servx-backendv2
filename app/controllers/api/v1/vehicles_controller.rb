class Api::V1::VehiclesController < Api::V1::ApiController
  before_action :secure_user_api,       only: [:create, :update, :destroy, :get_dealers, :create_address] 
  before_action :check_model,           only: [:create]  
  before_action :set_vehicle,           only: [:update, :destroy, :get_dealers]
  before_action :belongs_to_user,       only: [:update, :destroy, :get_dealers]


  def create
    vehicle_params[:reg_num].gsub!(/\s+/, '') unless vehicle_params[:reg_num].nil?
    vehicle_params[:reg_num].upcase! unless vehicle_params[:reg_num].nil?
    @vehicle = @user.vehicles.build(vehicle_params.slice(:reg_num, :vehiclemodel_id))
    if @vehicle.save
      @user.last_active_update
      @vehicle.add_attributes(@vehiclemodel)
      render json:{ code:1,response:{  msg: "Added #{@vehicle.model}", vehicle: @vehicle.as_json(only: ['id', 'reg_num'],methods: [:model, :image, :manufacturer_id])}}
    else
      render json:{code:0,response:{ msg:@vehicle.errors.messages}}
    end
  end

  def update
    vehicle_params[:reg_num].gsub!(/\s+/, '') unless vehicle_params[:reg_num].nil?
    vehicle_params[:reg_num].upcase! unless vehicle_params[:reg_num].nil?
    if @vehicle.update_attributes(reg_num: vehicle_params[:reg_num])      
      render json:{ code: 1,response: { msg: "successfully updated.", vehicle: @vehicle.as_json(only: ['id', 'reg_num'])}}
    else
      render json:{ code:0,response:{ msg:@vehicle.errors.messages }}
    end
  end

  def destroy
    @vehicle.deactivate
    render json:{ code:1,response:{msg: "Removed." }}
  end

  # def get_dealers
  #   location= Location.find_by(name: vehicle_params[:location].titleize) unless vehicle_params[:location].nil? 
  #   if location.nil?
  #     render json:{ code: 0,response:{ msg: "servX is coming to a dealership near you soon. Apologies for the inconvenience caused." }}
  #     return
  #   end
  #   alldealers=location.dealerships
  #   if alldealers.nil?
  #     render json:{ code:0,response:{ msg: "servX is coming to a dealership near you soon. Apologies for the inconvenience caused." }}
  #     return
  #   end      
  #   dealers=alldealers.where(manufacturer_id: @vehicle.manufacturer_id)
  #   if dealers.empty?
  #     render json:{ code: 0,response:{ msg: "servX is coming to a dealership near you soon. Apologies for the inconvenience caused." }}
  #   else
  #     ratings = Dealership.all_ratings(dealers)
  #     manufacturer = dealers.first.manufacturer
  #     image = manufacturer[:image]
  #     render json:{code: 1, response:{  msg: "Showing all dealerships.", dealers: dealers.as_json(only: ['image','latitude','longitude','address', 'name', 'id']),
  #                                                                                                                                                  ratings: ratings , logo: image}}
  #   end    
  # end

  def get_dealers_without_authentication
    location= Location.find_by(name: vehicle_params[:location].titleize) unless vehicle_params[:location].nil? 
    if location.nil?
      render json:{ code: 0,response:{ msg: "servX is coming to a dealership near you soon. Apologies for the inconvenience caused." }}
      return
    end
    alldealers=location.dealerships
    if alldealers.nil?
      render json:{ code:0,response:{ msg: "servX is coming to a dealership near you soon. Apologies for the inconvenience caused." }}
      return
    end      
    dealers=alldealers.where(manufacturer_id: vehicle_params[:manufacturer_id])
    if dealers.empty?
      render json:{ code: 0,response:{ msg: "servX is coming to a dealership near you soon. Apologies for the inconvenience caused." }}
    else
      Dealership.all_ratings(dealers)
      Dealership.check_premium(dealers)
      render json:{ code: 1,response:{  msg: "Showing all dealerships.", dealerships: dealers.as_json(only: ['image','latitude','longitude','address', 'name', 'id'],
                                                                                                                                               methods: [:rating, :premium])}}
    end    
  end  



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle
      @vehicle = Vehicle.find_by(id: params[:vehicle_id])
      if @vehicle.nil? || @vehicle.deactivated==true 
        render json:{ code: 0,response:{ msg:"Vehicle not found." }}
      end      
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vehicle_params
      params.permit(:manufacturer_id, :vehiclemodel_id, :reg_num, :location, :vehicle_id, :deactivated)
    end

    # Check if user is same as vehicle's user_id using vehicle id.
    def belongs_to_user
      if @vehicle.user_id!=@user[:id]
        render json:{ code: 0,response:{ msg: "Invalid access. This vehicle doesn't belong to you." }}
      end     
    end 

    #check model befor assigning
    def check_model
      @vehiclemodel = Vehiclemodel.find_by(id: params[:vehiclemodel_id])
      if @vehiclemodel.nil? 
        render json:{ code: 0,response:{ msg: "Model not found." }}
      end       
    end    
end