class OtpsController < ApplicationController

	def create
    #write("verify start")
		user = User.find_by(mobile: otp_params[:mobile])
    if user.nil?
      respond_to do |format|
        format.json { render json:{ code: 0, msg: "User does not exist."}}
      end
    elsif user && user.activated?
      respond_to do |format|
        format.json {render json:{ code: 1, msg: "Account already activated.", account_active: 1}}
      end
    elsif user && !user.activated? && user.otp==otp_params[:otp].to_i
      # user.activate
      # remember user
      # user.last_active_update
      # user.delete_otp
      user.many_updates
      user.welcome_me
      respond_to do |format|
        format.json {render json:{ code: 1, msg: "Successfully signed up!", user: user.as_json(only: ['name','id','remember_digest','mobile','email','address'])}}
      end
    else
      respond_to do |format|
		    format.json { render json: { code: 0, msg: "OTP doesn't match. Please try again."}}
      end
    end 
    #write("verify stop")
	end

  def update
    user = User.find_by(mobile: otp_params[:mobile])
    if user
    respond_to do |format|
      user.send_otp_from_controller
      format.json { render json: {code: 1, msg: "Sorry for the delay. You will receive your OTP shortly."}}
    end
    else
      respond_to do |format|
        format.json { render json: {code: 0, msg: "User does not exist."}}
      end      
    end
  end 

  private
  
    def otp_params
      params.permit(:mobile, :otp)
    end 	
end
