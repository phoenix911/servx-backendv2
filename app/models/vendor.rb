class Vendor < ActiveRecord::Base
  belongs_to :dealership
  has_many :sub_vendors, dependent: :destroy
  has_many :dealershipvendors, dependent: :destroy
  has_many :dealerships, through: :dealershipvendors
  validates :name, presence: {message: "Name can't be blank."}, length: { minimum: 3,message: "Name is not valid (minimum 3 characters)." }

  VALID_MOBILE_REGEX = /\A[789]\d{9}\z/
  validates :mobile, format: { with: VALID_MOBILE_REGEX ,message: "Invalid mobile number."},
  uniqueness: {message: "Mobile number has already been taken."}

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, length: { maximum: 255 },
  format: { with: VALID_EMAIL_REGEX, message: "Email is invalid." },
  uniqueness: { case_sensitive: false,message: "Email has already been taken." } 
  
  has_secure_password validations: false

  validates :password, presence: {message: "Password can't be blank."}, length: { minimum: 6, message: "Password is too short (minimum 6 characters)." }, on: :create

  validates :password, presence: {message: "Password can't be blank."}, length: { minimum: 6, message: "Password is too short (minimum 6 characters)." }, allow_nil: true, on: :update

  before_save   :downcase_email
  # Returns a random token.
  def Vendor.new_token
    SecureRandom.uuid
  end

  def remember
    self.remember_digest=Vendor.new_token
  end

  # Remembers a user in the database for use in persistent sessions.
  def update_remember
    update_attribute(:remember_digest, Vendor.new_token)
  end

   # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  def get_manufacturers
    manufacturers= Set.new
    self.dealerships.each do |dealership|
     manufacturers.add(dealership.manufacturer)
    end
    return manufacturers
  end 

  def set_password
    password=('a'..'z').to_a.sample(4).join + (0..9).to_a.sample(4).join
    self.password=password
    return password     
  end

  def set_relation(dealership_array)
    dealerships = eval(dealership_array)
    dealerships.each do |dealership|
      Dealershipvendor.create!(dealership_id: dealership, vendor_id: self.id)
    end  
  end

  #updates last activity time of user
  def last_active_update
    update_attribute(:last_active, Time.zone.now)
  end

  #updates last activity time of user
  def update_mutiple_values
    update_attributes(last_active: Time.zone.now,remember_digest: "1")
  end  

    private

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

end
