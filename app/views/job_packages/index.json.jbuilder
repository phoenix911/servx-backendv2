json.array!(@job_packages) do |job_package|
  json.extract! job_package, :id, :job_id, :package_id, :quantity
  json.url job_package_url(job_package, format: :json)
end
