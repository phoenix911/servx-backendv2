class AddMoreAttributesToWashBookings < ActiveRecord::Migration
  def change
    add_column :wash_bookings, :address, :text
    add_column :wash_bookings, :pickup_date, :date
    add_column :wash_bookings, :slot, :integer, default: 0
  end
end
