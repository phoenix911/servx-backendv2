# Preview all emails at http://localhost:3000/rails/mailers/sub_vendor_mailer
class SubVendorMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/sub_vendor_mailer/offer_mail
  def offer_mail
    SubVendorMailer.offer_mail
  end

end
