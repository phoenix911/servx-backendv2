class Api::V1::ManufacturersController < Api::V1::ApiController
  before_action :set_manufacturer, only: [:all_vehicles]

  # GET /manufacturers
  # GET /manufacturers.json
  def index
    @manufacturers = Manufacturer.order(:manufacturer)
    if !@manufacturers.empty?  
      render json:{ code: 1, response: {msg: "Listing all manufacturers.", manufacturers: @manufacturers.as_json(only: ['manufacturer', 'id', 'image'])}}
    else
      render json:{ code: 0,response:{ msg: "No manufacturers available." }}
    end
  end

  def all_vehicles
    allvehicles=@manufacturer.vehiclemodels.order(:name)
    if !allvehicles.empty?
      render json:{ code: 1,response: {  msg: "Showing all vehicles of this manufacturer.", allvehicles: allvehicles.as_json(only: ['name']) }}
    else
      render json:{ code: 0,response:{ msg:"No vehicles present of this manufacturer." }}
    end
  end

  def manufacturers_with_models
    @manufacturers = Manufacturer.order(:manufacturer)
    if !@manufacturers.empty?  
      render json:{ code: 1, img_url: "162.243.2.224/images/models/", response: {msg: "Listing all manufacturers with models.", manufacturers_with_models: @manufacturers.as_json(
                                                           include:{vehiclemodels: {only: ['name', 'id', 'image'],order: :name }},only: ['manufacturer', 'id', 'image'])}}
    else
      render json:{ code: 0,response:{ msg: "No manufacturers available." }}
    end 
  end

  def all_models
    models=Vehiclemodel.all.order(:name)
    if !models.empty?
      render json:{ code: 1,response: {  msg: "Showing all models.", models: models.as_json(only: ['name','manufacturer_id', 'image', 'id']) }}
    else
      render json:{ code: 0,response:{ msg:"No vehicles present." }}
    end
  end
  # # GET /manufacturers/new
  # def new
  #   @manufacturer = Manufacturer.new
  # end

  # # GET /manufacturers/1/edit
  # def edit
  # end

  # POST /manufacturers
  # POST /manufacturers.json
  # def create
  #   @manufacturer = Manufacturer.new(manufacturer_params)

  #   respond_to do |format|
  #     if @manufacturer.save
  #       format.html { redirect_to @manufacturer, notice: 'Manufacturer was successfully created.' }
  #       format.json { render :show, status: :created, location: @manufacturer }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @manufacturer.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # PATCH/PUT /manufacturers/1
  # # PATCH/PUT /manufacturers/1.json
  # def update
  #   respond_to do |format|
  #     if @manufacturer.update(manufacturer_params)
  #       format.html { redirect_to @manufacturer, notice: 'Manufacturer was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @manufacturer }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @manufacturer.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # DELETE /manufacturers/1
  # # DELETE /manufacturers/1.json
  # def destroy
  #   @manufacturer.destroy
  #   respond_to do |format|
  #     format.html { redirect_to manufacturers_url, notice: 'Manufacturer was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_manufacturer
      @manufacturer = Manufacturer.find_by(id: params[:manufacturer_id])
      if @manufacturer.nil?
        render json:{ code: 0,response:{  msg:"Manufacturer not found." }}
      end  
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def manufacturer_params
      params.permit(:manufacturer, :description, :manufacturer_id)
    end
end
