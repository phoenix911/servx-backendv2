class AddImageToVendors < ActiveRecord::Migration
  def change
    add_column :vendors, :image, :string, default: "default.png"
    add_column :sub_vendors, :image, :string, default: "default.png"
    add_column :service_advisors, :image, :string, default: "default.png"
    add_column :drivers, :image, :string, default: "default.png"
  end
end
