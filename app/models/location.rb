class Location < ActiveRecord::Base
	has_many :getdealerships, dependent: :destroy
	has_many :dealerships, through: :getdealerships
	validates :name, presence: true
	#validates :longitude, presence: true
	#validates :latitude, presence: true
	validates :state, presence: true

end
