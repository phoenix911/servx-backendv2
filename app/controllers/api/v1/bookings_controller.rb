class Api::V1::BookingsController < Api::V1::ApiController
	before_action :secure_user_api,        				only: [:create, :update, :destroy]
	before_action :set_booking,          					only: [:update, :destroy]
	before_action :check_booking,        					only: [:update, :destroy]
	before_action :check_vehicle_present,					only: [:create]
	before_action :check_user_and_vehicle,				only: [:create]
	before_action :check_dealership,							only: [:create]
	# before_action :check_address,									only: [:create]	
	before_action :check_package,									only: [:create]
	before_action :check_time,										only: [:create],if: :apply_offer?	
	before_action :check_offer,										only: [:create],if: :apply_offer?
	before_action :check_applies_on_dealership,		only: [:create],if: :apply_offer?
	#
	#
	#
	#
	#############################################################################
	###WHEN USER TRIES TO UPDATE A BOOKING CHECK VEHICLE'S DEACTIVATED BOOLEAN###
	#############################################################################
	#
	#
	#
	#
	def create
		@booking = @user.bookings.build(booking_params.except(:feedback, :booking_id, :apply, :offer,:reg_num,:vehiclemodel_id))
		if apply_offer?
			@booking.offer_id=@offer.id
		end
		if @booking.save
			@booking.generate_reference_code
			@user.last_active_update
			manufacturer=@dealership.manufacturer
			if booking_params[:jobs]
				@booking.send_user_booking_mail(@user,@vehicle,@dealership,manufacturer,@vehiclemodel)
				@booking.send_admin_booking_mail(@user,@vehicle,@dealership, @vehiclemodel)
				@booking.send_message_now(@user[:name], @user[:mobile])
				@booking.send_message_to_admin(@user[:name], "9838148268", @user[:mobile])
				if apply_offer?
					render json:{ code: 1,response: { msg: "Booking successfully created. Our executives will contact you shortly.", booking: @booking.as_json(only: ['ref_code','jobs','pickup_date', 'slot']), hash: 0, offer: "Offer used: #{@offer.name}" }}					
				else
					render json:{ code: 1,response:{  msg: "Booking successfully created. Our executives will contact you shortly.", booking: @booking.as_json(only: ['ref_code','jobs','pickup_date', 'slot']), hash: 0 }}
				end				
			elsif booking_params[:package_id]
				# @booking.send_user_booking_mail_with_package(@user,@vehicle,@dealership,manufacturer,@vehiclemodel,@package)
				# @booking.send_admin_booking_mail_with_package(@user,@vehicle,@dealership, @vehiclemodel)
				# send mail
				@booking.send_message_now(@user[:name], @user[:mobile])
				@booking.send_message_to_admin(@user[:name], "9838148268", @user[:mobile])
				if apply_offer?
					render json:{ code: 1,response: { msg: "Booking successfully created. Our executives will contact you shortly.", booking: @booking.as_json(include:{package:{only:['name','cost']}}, only: ['ref_code','pickup_date', 'slot']), hash: 0, offer: "Offer used: #{@offer.name}" }}					
				else
					render json:{ code: 1,response:{  msg: "Booking successfully created. Our executives will contact you shortly.", booking: @booking.as_json(include:{package:{only:['name','cost']}}, only: ['ref_code','pickup_date', 'slot']), hash: 0 }}
				end			
			end     
		else
			render json:{ code: 1,response:{  msg: @booking.errors.messages, hash: 1  }}
		end
	end


	# def update
	# 	respond_to do |format|
	# 		if !@user.check_address(booking_params[:address])
	# 			format.json { render json:{ code: "0", msg: @user.errors }}
	# 		end       
	# 		if @booking.update_attributes(booking_params.except(:remember_digest,:booking_id, :user_id, :vehicle_id, :dealership_id, :address))
	# 			format.json { render json:{ code: "1", msg: "Booking successfully updated.", booking: @booking}}
	# 		else
	# 			format.json { render json:{ code: "0", msg: @booking.errors.messages }}
	# 		end
	# 	end
	# end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_booking
			@booking = Booking.find_by(id: params[:booking_id])
			if @booking.nil? 
				render json:{ code: 0, response: {msg:"Booking not found."}}
			end
		end

		def check_booking
			if @booking.user_id!=@user[:id] 
				render json:{code: 0, response: {  msg:"Invalid access to this booking." }}
			end
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def booking_params
			@booking_params ||= params.permit(:vehicle_id, :dealership_id, :pickup_date, :urgent, :feedback, :jobs, :booking_id, :address, :offer, :apply, :package_id,:reg_num,:vehiclemodel_id, :slot)
		end

		def check_user_and_vehicle
			@vehicle||=Vehicle.find_by(id: booking_params[:vehicle_id])
			if @vehicle.nil? || @vehicle.deactivated==true      
				render json:{ code: 0,response:{  msg: "Vehicle not found."}}       
			elsif @vehicle.user_id!=@user[:id]
				render json:{ code: 0,response:{ msg:"Invalid access. You can not make a booking on this vehicle."}}
			end  
		end

		def check_dealership
			@dealership=Dealership.find_by(id: booking_params[:dealership_id])
			@vehiclemodel=@vehicle.vehiclemodel
			if !@dealership         
				render json:{ code:0,response:{  msg: "dealership not found."}}      
			elsif @dealership[:manufacturer_id]!=@vehiclemodel[:manufacturer_id]
				render json:{code:0, response:{  msg:"This dealership is not applicable on the vehicle you chose."}}
			end  
		end	

		def check_package
			if booking_params[:package_id]
				@package = Package.find_by(id: booking_params[:package_id])
				if @package.nil?
					render json:{ code:0, response:{  msg: "package not found."}}      
				elsif !@package.vehiclemodels.ids.include?(@vehiclemodel[:id])
					render json:{code:0, response:{  msg:"This package is not applicable on the vehicle you chose."}}
				end
			end  
		end				


		# def check_address
		# 	@address=Address.find_by(id: booking_params[:address_id])
		# 	if !@address        
		# 		render json:{ code:0,response:{  msg: "address not found."}}      
		# 	elsif @address[:user_id]!=@user[:id]
		# 		render json:{code:0, response:{  msg:"This address is not yours."}}
		# 	end  			
		# end	

		def check_time
			if !booking_params[:offer].nil?
				@offer= Offer.find_by(name: booking_params[:offer].upcase)
				if @offer.nil?
					render json:{ code: 0,response:{  msg:"Offer not found."}}		
				elsif !@offer.expires_at.nil?
					if @offer.expires_at < Time.zone.now
						render json:{ code: 0,response:{  msg: "This offer has expired."}}
					end
				end
			else
				render json:{code:0,response:{ msg:"Please apply an offer."}}
			end
		end

		def check_offer
			if booking_params[:jobs]
				jobs=booking_params[:jobs].split('#')			
				if jobs.size==1 && jobs.first == "Car wash"
					if @offer.wash == false
						render json:{ code: 0,response:{ msg: "This offer is not applicable on car wash." }}
					end
				elsif !jobs.include?("Car wash")
					if @offer.wash == true	
						render json:{ code: 0,response:{ msg: "This offer is not applicable without car wash"}}
					end
				end
			elsif booking_params[:package_id]
				if @offer.wash == true	
					render json:{ code: 0,response:{ msg: "This offer is not applicable on packages."}}
				end				
			end			
		end

		def check_applies_on_dealership
			if !@offer.all
				dealerships= @offer.dealerships
				if !dealerships.ids.include?(booking_params[:dealership_id])	
					render json:{code: 0,response:{  msg: "This offer is not applicable on this manufacturer"}}
				end	
			end	
		end	

		def apply_offer?
			if booking_params[:apply]== "false" || booking_params[:apply]== "0" || booking_params[:apply]== 0
				return false
			elsif booking_params[:apply]== "true" || booking_params[:apply]== "1" || booking_params[:apply]== 1
				return true 
			end
		end

		def check_vehicle_present
			if booking_params[:reg_num] && booking_params[:vehiclemodel_id]
				vehicle_array=@user.vehicles.where('reg_num = ? AND vehiclemodel_id = ?', booking_params[:reg_num], booking_params[:vehiclemodel_id])
				if vehicle_array.empty?
					@vehicle = @user.vehicles.build(booking_params.slice(:reg_num, :vehiclemodel_id))
					if @vehicle.save
						booking_params[:vehicle_id]=@vehicle[:id]
			    else
			      render json:{code:0,response:{ msg:@vehicle.errors.messages}}
			    end
			  else
			  	@vehicle=vehicle_array.first
			  	if @vehicle[:deactivated]=true
			  		@vehicle.update_attributes(deactivated: false)
			  	end
			  	booking_params[:vehicle_id]=@vehicle[:id]
			  end
			end
		end
end
