class ReviewsController < ApplicationController
  #before_action :set_review, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:create]
  before_action :check_user_and_booking, only: [:create]

  # GET /reviews
  # GET /reviews.json
  # def index
  #   @reviews = Review.all
  # end

  # # GET /reviews/1
  # # GET /reviews/1.json
  # def show
  # end

  # POST /reviews
  # POST /reviews.json
  def create
    @review = Review.new(review_params.except(:remember_digest))
    respond_to do |format|
      if @review.save
        if @review.rating < 4
          format.json {render json:{ code: "1", msg: "Please do let us know what issues you faced in the feedback form."}}
        else
          format.json {render json:{ code: "1", msg: "Rated #{@review.rating} stars for this dealership."}}
        end
      else
        format.json {render json: {code: "0", msg: @review.errors.messages}}
      end
    end
  end

  # # PATCH/PUT /reviews/1
  # # PATCH/PUT /reviews/1.json
  # def update
  #   respond_to do |format|
  #     if @review.update(review_params)
  #       format.html { redirect_to @review, notice: 'Review was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @review }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @review.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /reviews/1
  # DELETE /reviews/1.json
  # def destroy
  #   @review.destroy
  #   respond_to do |format|
  #     format.html { redirect_to reviews_url, notice: 'Review was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    # def set_review
    #   @review = Review.find(params[:id])
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
      #add review to  params when you enable reviews.
      params.permit(:rating, :user_id, :booking_id, :dealership_id, :remember_digest)
    end

    def check_user_and_booking
      @booking= Booking.find_by(id: review_params[:booking_id])
      if @booking.nil?
        respond_to do |format|           
          format.json { render json:{ code: "0", msg: "Booking not found." }} 
        end 
      elsif @booking.user_id!=review_params[:user_id].to_i || @booking.dealership_id!=review_params[:dealership_id].to_i
        respond_to do |format|           
          format.json { render json:{ code: "0", msg: "Invalid rating." }} 
        end 
      elsif @booking.review
        respond_to do |format|           
          format.json { render json:{ code: "0", msg: "You have already given a rating for this dealershrship through this booking." }} 
        end         
      end    
    end              
end
