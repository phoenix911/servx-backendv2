class AddRefCodeToWashBooking < ActiveRecord::Migration
  def change
    add_column :wash_bookings, :ref_code, :string
  end
end
