class CreateDealerships < ActiveRecord::Migration
  def change
    create_table :dealerships do |t|
      t.string :name
      t.string :address
      t.string :image
      t.string :gm_name
      t.string :gm_email
      t.string :gm_phone
      t.string :wm_name
      t.string :wm_email
      t.string :wm_phone
      t.string :ccm_name
      t.string :ccm_email
      t.string :ccm_phone            
      t.float :latitude
      t.float :longitude
      t.boolean :activated
      t.string :email
      t.references :manufacturer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
