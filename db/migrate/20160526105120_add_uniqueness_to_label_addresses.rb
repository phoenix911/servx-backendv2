class AddUniquenessToLabelAddresses < ActiveRecord::Migration
  def change
  	add_index :addresses, :label, unique: true
  end
end
