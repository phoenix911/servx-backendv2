// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

jQuery(document).ready(function($){

	$("#vid_btn").on("click",function(e){
		e.preventDefault();
		$("#video")[0].play();
		$("body").addClass('video_playing');
		$("#video").prop('controls',true);
		$("html,body").animate({
			'scrollTop':'0'
		});
	});

	$("#close_vid").on("click",function(){
		$("#video")[0].pause();
		$("body").removeClass('video_playing');
		$("#video").prop('controls',false);
	});

	$("#know_more").on("click",function(){
		$("body,html").animate({
			'scrollTop':$("#frame2").offset().top
		});
		$("body").addClass('scrollable');
	});

});


//if you don't know what this is, then get out of my code!!!
window.onbeforeunload=function(){
	$(window).scrollTop(0);
}