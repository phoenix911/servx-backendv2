require 'test_helper'

class ServiceAdvisorsControllerTest < ActionController::TestCase
  setup do
    @service_advisor = service_advisors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:service_advisors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create service_advisor" do
    assert_difference('ServiceAdvisor.count') do
      post :create, service_advisor: { email: @service_advisor.email, mobile: @service_advisor.mobile, name: @service_advisor.name, password_digest: @service_advisor.password_digest, remember_digest: @service_advisor.remember_digest, vendor_mobile: @service_advisor.vendor_mobile }
    end

    assert_redirected_to service_advisor_path(assigns(:service_advisor))
  end

  test "should show service_advisor" do
    get :show, id: @service_advisor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @service_advisor
    assert_response :success
  end

  test "should update service_advisor" do
    patch :update, id: @service_advisor, service_advisor: { email: @service_advisor.email, mobile: @service_advisor.mobile, name: @service_advisor.name, password_digest: @service_advisor.password_digest, remember_digest: @service_advisor.remember_digest, vendor_mobile: @service_advisor.vendor_mobile }
    assert_redirected_to service_advisor_path(assigns(:service_advisor))
  end

  test "should destroy service_advisor" do
    assert_difference('ServiceAdvisor.count', -1) do
      delete :destroy, id: @service_advisor
    end

    assert_redirected_to service_advisors_path
  end
end
