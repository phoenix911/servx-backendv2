class AddDescriptionAndTermsToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :description, :text
    add_column :offers, :terms, :text
  end
end
