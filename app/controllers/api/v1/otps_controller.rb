class Api::V1::OtpsController < Api::V1::ApiController

	def create
		user = User.find_by(mobile: otp_params[:mobile])
    if user.nil?
      render json:{ code:0, response:{ msg: "User does not exist."}}
    elsif user && user.activated?
      render json:{code:1, response:{ msg: "Account already activated.", account_active: 1}}
    elsif user && !user.activated? && user.otp==otp_params[:otp].to_i
      user.many_updates
      user.welcome_me
      render json:{ code:1,response:{ msg: "Successfully signed up!", user: user.as_json(only: ['name','id','remember_digest','mobile','email','address'])}}
    else
		  render json: {code:0,response:{ msg:"OTP doesn't match. Please try again."}}
    end 
	end

  def update
    user = User.find_by(mobile: otp_params[:mobile])
    if user
      user.send_otp_from_controller
      render json: {code:1,response:{msg: "Sorry for the delay. You will receive your OTP shortly."}}
    else
      render json: {code: 0, response:{ msg:"User does not exist."}}
    end
  end 

  private
  
    def otp_params
      params.permit(:mobile, :otp)
    end 	
end
