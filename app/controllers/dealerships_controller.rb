class DealershipsController < ApplicationController
  before_action :set_dealership, only: [:show, :edit, :update, :destroy]

  # GET /dealerships
  # GET /dealerships.json
  def index
    @dealerships = Dealership.all
    respond_to do |format|
      if !@dealerships.empty?
        format.json { render json:{ code: 1, msg: "listing all dealerships.", dealerships: @dealerships.as_json(only: ['name', 'id', 'address', 'image']) }}
      else
        format.json { render json:{ code: 0, msg: "No dealerships available." }}
      end
    end    
  end

  def all_reviews
    @dealership = Dealership.find_by(id: dealership_params[:dealership_id])
    if @dealership.nil? 
      respond_to do |format|
        format.json { render json:{ code: 0, msg: "Dealership not found." }}
      end    
    else
      rating=@dealership.get_rating
      respond_to do |format|
        if rating==0.0
          #rating= "N/A"
          format.json { render json:{ code: 1, msg: "No ratings yet.", rating: rating }}
        else
          format.json { render json:{ code: 1, msg: "Showing rating for this dealership.", rating: rating }}
        end 
      end
    end       
  end
  # # GET /dealerships/1
  # # GET /dealerships/1.json
  # def show
  # end

  # # GET /dealerships/new
  # def new
  #   @dealership = Dealership.new
  # end

  # # GET /dealerships/1/edit
  # def edit
  # end

  # # POST /dealerships
  # # POST /dealerships.json
  # def create
  #   @dealership = Dealership.new(dealership_params)

  #   respond_to do |format|
  #     if @dealership.save
  #       format.html { redirect_to @dealership, notice: 'Dealership was successfully created.' }
  #       format.json { render :show, status: :created, location: @dealership }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @dealership.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # PATCH/PUT /dealerships/1
  # # PATCH/PUT /dealerships/1.json
  # def update
  #   respond_to do |format|
  #     if @dealership.update(dealership_params)
  #       format.html { redirect_to @dealership, notice: 'Dealership was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @dealership }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @dealership.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # DELETE /dealerships/1
  # # DELETE /dealerships/1.json
  # def destroy
  #   @dealership.destroy
  #   respond_to do |format|
  #     format.html { redirect_to dealerships_url, notice: 'Dealership was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # # Use callbacks to share common setup or constraints between actions.
    # def set_dealership
    #   @dealership = Dealership.find_by(id: params[:id])
    #   if @dealership.nil? 
    #     respond_to do |format|
    #       format.json { render json:{ code: "0", msg: "Dealership not found." }}
    #     end
    #   end 
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dealership_params
      # more to be added.
      params.permit(:name, :contact_person, :latitude, :longitude, :activated, :email, :manufacturers_id, :dealership_id)
    end
end
