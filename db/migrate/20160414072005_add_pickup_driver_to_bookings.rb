class AddPickupDriverToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :pickup_driver, :integer
  end
end
