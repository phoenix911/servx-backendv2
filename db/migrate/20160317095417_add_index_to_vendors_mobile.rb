class AddIndexToVendorsMobile < ActiveRecord::Migration
  def change
  	add_index :vendors, :mobile, unique: true
  end
end
