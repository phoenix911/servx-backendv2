module SessionsHelper
  
  # remembers user
  def remember(user)
    user.remember
  end
    
  # Forgets a persistent session.
  def forget(user)
    user.forget
  end
    
end
