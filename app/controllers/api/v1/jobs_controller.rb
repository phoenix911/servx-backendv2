class JobsController < ApplicationController
  before_action :set_job, only: [:show, :edit, :update, :destroy]

  # GET /jobs
  # GET /jobs.json
  def index
    @jobs = Job.order(:name)
    if !@jobs.empty?
      render json:{ code: 1, response:{ msg: "listing all jobs.", jobs: @jobs.as_json(only: ['name']) }}
    else
      render json:{ code: 0, response:{ msg: "No jobs available." }}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params.require(:job).permit(:name, :category)
    end
end
