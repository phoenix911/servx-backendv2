class JobPackagesController < ApplicationController
  before_action :set_job_package, only: [:show, :edit, :update, :destroy]

  # GET /job_packages
  # GET /job_packages.json
  def index
    @job_packages = JobPackage.all
  end

  # GET /job_packages/1
  # GET /job_packages/1.json
  def show
  end

  # GET /job_packages/new
  def new
    @job_package = JobPackage.new
  end

  # GET /job_packages/1/edit
  def edit
  end

  # POST /job_packages
  # POST /job_packages.json
  def create
    @job_package = JobPackage.new(job_package_params)

    respond_to do |format|
      if @job_package.save
        format.html { redirect_to @job_package, notice: 'Job package was successfully created.' }
        format.json { render :show, status: :created, location: @job_package }
      else
        format.html { render :new }
        format.json { render json: @job_package.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_packages/1
  # PATCH/PUT /job_packages/1.json
  def update
    respond_to do |format|
      if @job_package.update(job_package_params)
        format.html { redirect_to @job_package, notice: 'Job package was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_package }
      else
        format.html { render :edit }
        format.json { render json: @job_package.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_packages/1
  # DELETE /job_packages/1.json
  def destroy
    @job_package.destroy
    respond_to do |format|
      format.html { redirect_to job_packages_url, notice: 'Job package was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_package
      @job_package = JobPackage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_package_params
      params.require(:job_package).permit(:job_id, :package_id, :quantity)
    end
end
