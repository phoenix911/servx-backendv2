class CreateGetdealerships < ActiveRecord::Migration
  def change
    create_table :getdealerships do |t|
      t.references :dealership, index: true, foreign_key: true
      t.references :location, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
