json.array!(@vehiclemodels) do |vehiclemodel|
  json.extract! vehiclemodel, :id, :name, :manufacturer_id
  json.url vehiclemodel_url(vehiclemodel, format: :json)
end
