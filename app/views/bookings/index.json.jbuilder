json.array!(@bookings) do |booking|
  json.extract! booking, :id, :vehicle_id, :dealer_id, :pickup_time, :drop_time, :urgent, :reference_code, :confirmed, :feedback
  json.url booking_url(booking, format: :json)
end
