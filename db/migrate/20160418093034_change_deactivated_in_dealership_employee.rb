class ChangeDeactivatedInDealershipEmployee < ActiveRecord::Migration
  def change
  	change_column :drivers, :deactivated, :boolean, default: :false
  	change_column :sub_vendors, :deactivated, :boolean, default: :false
  	change_column :service_advisors, :deactivated, :boolean, default: :false
  end
end
