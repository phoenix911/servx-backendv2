class RemoveManufacturerIdFromGetoffers < ActiveRecord::Migration
  def change
  	remove_column :getoffers, :manufacturer_id
  end
end
