 class UsersController < ApplicationController
  before_action :logged_in_user, only: [:user_vehicles, :user_notifications, :user_bookings, :send_otp, :verify, :resend, :update]
  
  def create
   # write("signup start")
  	@user = User.new(user_params.except(:temp_otp))
    if @user.save
      respond_to do |format|
        format.json { render json: {code: 1, msg: "You will receive your OTP shortly."}}
      end
    else
      respond_to do |format|
        # if @user.errors[:mobile]==["Mobile number has already been taken."] && @user.errors[:email]==["Email has already been taken."]
        #   format.json { render json: {code: 0, msg: "Account already exists.", account_exists: 1}}
        # else
		      format.json { render json: {code: 0, msg: @user.errors.messages}}
       # end
      end
    end
    #write("signup stop")
  end

  # def show
  #   respond_to do |format|
  #     format.json { render json:{ code: "1", msg: "Showing user.", user: @user }}
  #   end
  # end  

  def send_otp
    respond_to do |format|
      @user.send_temp_otp
      format.json {render json:{ code: "1", msg: "You will receive your OTP shortly."}}
    end
  end

  def verify
    if @user.temp_otp==user_params[:temp_otp].to_i
      respond_to do |format|
        format.json {render json:{ code: "1", msg: "OTP verified. You can edit your account",user: @user}}
      end
    else
      respond_to do |format|
        format.json {render json:{ code: "0", msg: "OTP doesn't match.Please retry."}}
      end
    end
  end

  def resend
    @user.send_temp_otp
    respond_to do |format|
      format.json {render json:{ code: "1", msg: "Sorry for the delay. You will receive your OTP shortly.", user: @user}}
    end
  end

  def update
    if @user.temp_otp==user_params[:temp_otp].to_i
      respond_to do |format|
        if @user.update_attributes(user_params.except(:remember_digest, :temp_otp))
          @user.delete_temp_otp
          remember @user
          format.json { render json:{ code: "1", msg: "Profile successfully updated.", user: @user}}
        else
          format.json { render json:{ code: "0", msg: @user.errors.messages }}
        end
      end
    else
      respond_to do |format|
        format.json {render json:{ code: "0", msg: "You can not come here without generating OTP."}}
      end
    end  
  end

  def user_vehicles
    vehicles=@user.vehicles.where(deactivated: false).order('id DESC')
    respond_to do |format|
      if !vehicles.empty?
        format.json { render json:{ code: 1, msg: "Showing all your vehicles.", vehicles: vehicles.as_json(except: ['created_at','updated_at','user_id','manufacturer_id','deactivated']), vehicle_empty: 0 }}
      else
        format.json { render json:{ code: 1, msg: "No vehicles present.", vehicles: vehicles, vehicle_empty: 1 }}
      end  
    end
  end

  def user_bookings
    bookings=@user.bookings
    respond_to do |format|
      if !bookings.empty?
        format.json { render json:{ code: 1, msg: "Showing all your bookings.", bookings: bookings}}
      else
        format.json { render json:{ code: 0, msg: "No bookings present.", bookings: bookings }}
      end  
    end
  end

  def user_notifications
    notifications=@user.notifications
    respond_to do |format|
      if !notifications.empty?
        format.json { render json:{ code: 1, msg: "Showing all your notifications.", notifications: notifications.as_json(only: 'text')}}
      else
        format.json { render json:{ code: 0, msg: "No notifications present.", notifications: notifications}}
      end  
    end
  end  

  # REMOVE THIS ONE IN PRODUCTION
  def destroy
    user = User.find_by(mobile: params[:mobile])
    respond_to do |format|
      if user
        name= user.name
        user.destroy
        format.json { render json:{ code: 1, msg: "Removed #{name}" }}  
      else
        format.json { render json:{ code: 0, msg: "n/a" }}  
      end
    end
  end
  
  # REMOVE THIS ONE IN PRODUCTION
  def mera_otp
    user = User.find_by(mobile: params[:mobile])
    otp= user.otp
    respond_to do |format|
      format.json { render json:{ code: "1", msg: "OTP-#{otp}" }}
    end   
  end

  def static
  end

private
    # Use callbacks to share common setup or constraints between actions.
    def user_params
      params.permit(:name, :email, :password, :mobile, :temp_otp, :gcm_token, :apns_token)
    end	
end

