class Driver < ActiveRecord::Base

	validates :name, presence: {message: "Name can't be blank."}, length: { minimum: 3,message: "Name is not valid (minimum 3 characters)." }

	VALID_MOBILE_REGEX = /\A[789]\d{9}\z/
	validates :mobile, format: { with: VALID_MOBILE_REGEX ,message: "Invalid mobile number."},
	uniqueness: {message: "Mobile number has already been taken."}

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
	validates :email, length: { maximum: 255 },
	format: { with: VALID_EMAIL_REGEX, message: "Email is invalid." },
	uniqueness: { case_sensitive: false,message: "Email has already been taken." } 

	has_secure_password validations: false

	validates :password, presence: {message: "Password can't be blank."}, length: { minimum: 6, message: "Password is too short (minimum 6 characters)." }, on: :create

	validates :password, presence: {message: "Password can't be blank."}, length: { minimum: 6, message: "Password is too short (minimum 6 characters)." }, allow_nil: true, on: :update

  validates :dealership_id, presence: {message: "Dealer not found."}

  belongs_to :dealership

  has_many :picked_bookings, class_name: 'Booking', foreign_key: 'pickup_driver'

  has_many :dropped_bookings, class_name: 'Booking', foreign_key: 'drop_driver'  

  before_save   :downcase_email  
	# Returns a random token.
	def Driver.new_token
	SecureRandom.uuid
	end

	def remember
	self.remember_digest=Driver.new_token
	end

	# Remembers a user in the database for use in persistent sessions.
	def update_remember
	update_attribute(:remember_digest, Driver.new_token)
	end

	# Forgets a driver.
	def forget
	update_attribute(:remember_digest, nil)
	end

	 #deactivates driver.
  def deactivate
    update_attribute(:deactivated, true)
  end 

  	 #deactivates vendor.
  def update_status(status)
  	if status==1
    	update_attribute(:available, true)
		elsif status==0
			update_attribute(:available, false)
		end
  end 

  def bookings_count
  	totalCount=0
  	totalCount=Booking.where('pickup_driver = ? OR drop_driver = ?', self.id, self.id).count
  	return totalCount
  end
  
  private

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end	

end
