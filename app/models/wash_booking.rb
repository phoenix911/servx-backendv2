class WashBooking < ActiveRecord::Base
  belongs_to :user
  belongs_to :wash_package
  validates :user_id, presence: {message: "User not found."}
  validates :wash_package_id, presence: {message: "Package not found."}
  validates :address, presence: {message: "Address not found."}, length: { minimum: 4, message: "Address is not valid (minimum 4 characters)." }
  validates :pickup_date, presence: {message: "Pick up date not found."}
  validates :slot, presence: {message: "Time slot not found."}
  before_save :check_date
  attr_accessor :get_package
  def check_date
    if self.pickup_date <= Date.today
      self.errors.add(:pickup_date,"Select a date ahead from today's date.")
      return false
    elsif self.pickup_date > Date.today+2.months
      self.errors.add(:pickup_date,"Selected date can not be ahead of 2 months from current date.")
      return false
    end 
  end

  def send_message_now(name,mobile)
    msg= "Hi%20#{name}.%20Your%20Booking%20has%20been%20confirmed%28reference%2Dcode:#{self.ref_code}%29.%20We%20will%20call%20you%20back%20shortly."
    Net::HTTP.get(URI("http://trans.kapsystem.com/api/v3/index.php?method=sms&api_key=A261352e7111244296e599e1b25b3fd7d&to=#{mobile}&sender=ISERVX&message=#{msg}&format=json&custom=1,2&flash=0"))
  end

  def send_message_to_admin(name,mobile,user_mobile)
    msg= "#{name}, #{user_mobile} has booked a service. Please check mail!"
    Net::HTTP.get(URI("http://trans.kapsystem.com/api/v3/index.php?method=sms&api_key=A261352e7111244296e599e1b25b3fd7d&to=#{mobile}&sender=ISERVX&message=#{msg}&format=json&custom=1,2&flash=0"))    
  end
  
  def generate_code
    ('A'..'Z').to_a.sample(4).join + (0..9).to_a.sample(4).join
  end

  def generate_ref_code
    code=generate_code
    idInHex=self.id.to_s(16)
    update_attribute(:ref_code,"#{code}#{idInHex}WASH")      
  end 
end
