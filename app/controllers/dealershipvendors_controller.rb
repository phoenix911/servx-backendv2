class DealershipvendorsController < ApplicationController
  before_action :set_dealershipvendor, only: [:show, :edit, :update, :destroy]

  # GET /dealershipvendors
  # GET /dealershipvendors.json
  def index
    @dealershipvendors = Dealershipvendor.all
  end

  # GET /dealershipvendors/1
  # GET /dealershipvendors/1.json
  def show
  end

  # GET /dealershipvendors/new
  def new
    @dealershipvendor = Dealershipvendor.new
  end

  # GET /dealershipvendors/1/edit
  def edit
  end

  # POST /dealershipvendors
  # POST /dealershipvendors.json
  def create
    @dealershipvendor = Dealershipvendor.new(dealershipvendor_params)

    respond_to do |format|
      if @dealershipvendor.save
        format.html { redirect_to @dealershipvendor, notice: 'Dealershipvendor was successfully created.' }
        format.json { render :show, status: :created, location: @dealershipvendor }
      else
        format.html { render :new }
        format.json { render json: @dealershipvendor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dealershipvendors/1
  # PATCH/PUT /dealershipvendors/1.json
  def update
    respond_to do |format|
      if @dealershipvendor.update(dealershipvendor_params)
        format.html { redirect_to @dealershipvendor, notice: 'Dealershipvendor was successfully updated.' }
        format.json { render :show, status: :ok, location: @dealershipvendor }
      else
        format.html { render :edit }
        format.json { render json: @dealershipvendor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dealershipvendors/1
  # DELETE /dealershipvendors/1.json
  def destroy
    @dealershipvendor.destroy
    respond_to do |format|
      format.html { redirect_to dealershipvendors_url, notice: 'Dealershipvendor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dealershipvendor
      @dealershipvendor = Dealershipvendor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dealershipvendor_params
      params.require(:dealershipvendor).permit(:vendor_id, :dealership_id)
    end
end
