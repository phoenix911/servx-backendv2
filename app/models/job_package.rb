class JobPackage < ActiveRecord::Base
  belongs_to :job
  belongs_to :package
  validates :package_id, presence: {message: "Package not found."}
  validates :job_id, presence: {message: "Selected job not found."}

	def get_job
		job=self.job
		return job[:name]
	end    
end
