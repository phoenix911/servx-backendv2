class WashPackage < ActiveRecord::Base
	has_many :wash_bookings, dependent: :destroy
	default_scope -> { order(:model, cost: :asc) }
end
