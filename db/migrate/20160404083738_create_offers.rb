class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :name
      t.string :image
      t.datetime :expires_at

      t.timestamps null: false
    end
  end
end
