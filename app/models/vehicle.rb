class Vehicle < ActiveRecord::Base
  belongs_to :user
  belongs_to :vehiclemodel
  has_many :bookings, dependent: :destroy
  validates :user_id, presence: {message: "User not found."}
  validates :vehiclemodel_id, presence: {message: "Model not found."}
  VALID_REGISTRATION_NUMBER_REGEX= /\A[a-zA-Z0-9]+\z/
  validates :reg_num, presence: {message: "Registration number can't be blank."},
  length: { minimum: 4, maximum: 11, too_short: "Registration number is not valid (minimum 4 characters without counting spaces).",
  too_long: "Registration number is not valid (maximum 11 characters without counting spaces)." },
   format: { with: VALID_REGISTRATION_NUMBER_REGEX ,message: "Only alphanumeric characters are allowed for registration number."}
  attr_accessor :model
  attr_accessor :image
  attr_accessor :manufacturer_id

  # def update_image(setImage)
  #   name= "#{setImage.gsub(/\s+/, '-')}.jpg" 
  #   path= ""
  #   if Rails.env=="development"
  #     path="localhost:3000/images/models/"
  #   elsif Rails.env== "production"
  #     path= "162.243.2.224/images/models/"
  #   end
  #   self.image="#{path}#{name}"
  # end

  #deactivates vehicle.
  def deactivate
    update_attributes(deactivated: true)
  end 

  def add_attributes(model)
    self.image=model.image
    self.model=model.name
    self.manufacturer_id=model.manufacturer_id
  end

  def image_and_model
    model=self.vehiclemodel
    self.image=model.image
    self.model=model.name
    self.manufacturer_id=model.manufacturer_id
    return nil
  end

  def active_vehicles
    Vehicle.where(deactivated: true)
  end
end
