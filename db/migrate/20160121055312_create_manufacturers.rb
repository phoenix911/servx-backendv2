class CreateManufacturers < ActiveRecord::Migration
  def change
    create_table :manufacturers do |t|
      t.string :manufacturer
      t.text :description
      t.string :image


      t.timestamps null: false
    end
  end
end
