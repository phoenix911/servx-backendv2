class AddOfferIdToBookings < ActiveRecord::Migration
  def change
    add_reference :bookings, :offer, index: true, foreign_key: true
  end
end
