class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.references :vehiclemodel, index: true, foreign_key: true
      t.references :job, index: true, foreign_key: true
      t.float :price

      t.timestamps null: false
    end
  end
end
