json.array!(@admins) do |admin|
  json.extract! admin, :id, :name, :mobile, :email, :password_digest, :image, :remember_digest
  json.url admin_url(admin, format: :json)
end
