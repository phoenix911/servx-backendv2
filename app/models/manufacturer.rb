class Manufacturer < ActiveRecord::Base
	has_many :dealerships, dependent: :destroy
	has_many :vehiclemodels, dependent: :destroy
	default_scope -> { order(:manufacturer) }
end
