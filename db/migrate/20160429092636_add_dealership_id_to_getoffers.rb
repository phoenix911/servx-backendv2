class AddDealershipIdToGetoffers < ActiveRecord::Migration
  def change
    add_reference :getoffers, :dealership, index: true, foreign_key: true
  end
end
