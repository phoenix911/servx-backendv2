json.array!(@notifications) do |notification|
  json.extract! notification, :id, :text, :user_id
  json.url notification_url(notification, format: :json)
end
