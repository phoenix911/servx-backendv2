class ServiceAdvisorsController < ApplicationController
  before_action :logged_in_service_advisor, only: [:signout]

  # GET /service_advisors
  # GET /service_advisors.json
  def index
    @service_advisors = ServiceAdvisor.all
  end


  def signin
    @service_advisor= ServiceAdvisor.find_by(mobile: service_advisor_params[:mobile])
    respond_to do |format|
      if @service_advisor && @service_advisor.authenticate(service_advisor_params[:password])
        @service_advisor.update_remember
        format.json { render json: {code: 1, msg: "Successfully logged in!", service_advisor: @service_advisor.as_json(only: ['name','id','remember_digest','mobile','email'])}}
      elsif @service_advisor
        format.json { render json: {code: 0, msg: "Invalid password"}}
      else
        format.json { render json: {code: 0, msg: "Invalid email/password combination."}}
      end
    end  
  end

  def signout
    @service_advisor.forget
    respond_to do |format|
      format.json { render json: {code: 1, msg: "Successfully logged out."}}       
    end
  end  
  # # GET /service_advisors/1
  # # GET /service_advisors/1.json
  # def show
  # end

  # # GET /service_advisors/new
  # def new
  #   @service_advisor = ServiceAdvisor.new
  # end

  # # GET /service_advisors/1/edit
  # def edit
  # end

  # # POST /service_advisors
  # # POST /service_advisors.json
  # def create
  #   @service_advisor = ServiceAdvisor.new(service_advisor_params)

  #   respond_to do |format|
  #     if @service_advisor.save
  #       format.html { redirect_to @service_advisor, notice: 'Service advisor was successfully created.' }
  #       format.json { render :show, status: :created, location: @service_advisor }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @service_advisor.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # PATCH/PUT /service_advisors/1
  # # PATCH/PUT /service_advisors/1.json
  # def update
  #   respond_to do |format|
  #     if @service_advisor.update(service_advisor_params)
  #       format.html { redirect_to @service_advisor, notice: 'Service advisor was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @service_advisor }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @service_advisor.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # DELETE /service_advisors/1
  # # DELETE /service_advisors/1.json
  # def destroy
  #   @service_advisor.destroy
  #   respond_to do |format|
  #     format.html { redirect_to service_advisors_url, notice: 'Service advisor was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_advisor
      @service_advisor = ServiceAdvisor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_advisor_params
      #change params accordingly
      params.permit(:name, :email, :password, :remember_digest, :mobile)
    end
end
