require 'test_helper'

class DealershipMailerTest < ActionMailer::TestCase
  test "dealership_booking_detail" do
    mail = DealershipMailer.dealership_booking_detail
    assert_equal "Dealership booking detail", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
