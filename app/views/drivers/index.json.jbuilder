json.array!(@drivers) do |driver|
  json.extract! driver, :id, :name, :email, :password_digest, :remember_digest, :mobile, :vendor_mobile
  json.url driver_url(driver, format: :json)
end
