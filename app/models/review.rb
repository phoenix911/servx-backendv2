class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :dealership
  #validates :review, length: { minimum: 3, message: "Review should be atleast 3 characters long." }
  validates :rating, presence: {message: "Please enter rating."}
  validates :user_id, presence: {message: "User not found."}
  validates :booking_id, presence: {message: "Booking not found."}
  validates :dealership_id, presence: {message: "Dealership not found."}
	before_save :check_rating

	def check_rating
    if self.rating < 1 && self.rating > 5
    	self.errors.add(:rating, "Rating can not be more than 5 and less than 1 star")
    	return false
    end 		
	end

end
