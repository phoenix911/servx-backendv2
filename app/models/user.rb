class User < ActiveRecord::Base
  require 'net/http'
  has_many :vehicles, dependent: :destroy
  has_many :active_vehicles, -> { where deactivated: false }, class_name: 'Vehicle', foreign_key: 'user_id'
  has_many :bookings, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_many :addresses, dependent: :destroy
  has_many :wash_bookings, dependent: :destroy
  before_save   :downcase_email
  before_create :send_otp
  validates :name, presence: {message: "Name can't be blank."}, length: { minimum: 3,message: "Name is not valid (minimum 3 characters)." }
  VALID_MOBILE_REGEX = /\A[789]\d{9}\z/
  validates :mobile, format: { with: VALID_MOBILE_REGEX ,message: "Invalid mobile number."},
  uniqueness: {message: "Mobile number has already been taken."}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, length: { maximum: 255 },
  format: { with: VALID_EMAIL_REGEX, message: "Email is invalid." },
  uniqueness: { case_sensitive: false,message: "Email has already been taken." }
  has_secure_password validations: false
  validates :password, presence: {message: "Password can't be blank."}, length: { minimum: 6, message: "Password is too short (minimum 6 characters)." }, on: :create
  validates :password, presence: {message: "Password can't be blank."}, length: { minimum: 6, message: "Password is too short (minimum 6 characters)." }, allow_nil: true, on: :update

  # Activates an account.
  def activate
    update_columns(activated: true, activated_at: Time.zone.now)
  end

  #updates last activity time of user
  def last_active_update
    update_attribute(:last_active, Time.zone.now)
  end

  def update_gcm_token(token)
    update_attribute(:gcm_token, token)
  end

  def update_apns_token(token)
    update_attribute(:apns_token, token)
  end
  
  # Returns a random token.
  def User.new_token
    SecureRandom.uuid
  end
  
  # Remembers a user in the database for use in persistent sessions.
  def remember
    token= User.new_token
    user=User.find_by(remember_digest: token)
    while user do
      token= User.new_token
      user=User.find_by(remember_digest: token)      
    end
    update_attribute(:remember_digest, token)
  end
  
   # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

   # Sends temp otp.
  def send_temp_otp
    temp_otp=Random.rand(1000..9999)
    number=self.mobile
    update_attribute(:temp_otp, temp_otp)
    name=self.name
    msg= "Hi%20#{name}%2C%20welcome%20to%20ServX.%20Your%20one%20time%20password%20is%20#{temp_otp}."
    Net::HTTP.get(URI("http://trans.kapsystem.com/api/v3/index.php?method=sms&api_key=A261352e7111244296e599e1b25b3fd7d&to=#{number}&sender=ISERVX&message=#{msg}&format=json&custom=1,2&flash=0"))
  end

  # Sends otp.
  def send_otp
    otp=Random.rand(1000..9999)
    number=self.mobile
    self.otp=otp
    name=self.name
    msg= "Hi%20#{name}%2C%20welcome%20to%20ServX.%20Your%20one%20time%20password%20is%20#{otp}."
    Net::HTTP.get(URI("http://trans.kapsystem.com/api/v3/index.php?method=sms&api_key=A261352e7111244296e599e1b25b3fd7d&to=#{number}&sender=ISERVX&message=#{msg}&format=json&custom=1,2&flash=0"))
  end
  
  # Sends otp from controller(Use this for controller).
  def send_otp_from_controller
    otp=Random.rand(1000..9999)
    number=self.mobile
    update_attribute(:otp, otp)
    name=self.name
    msg= "Hi%20#{name}%2C%20welcome%20to%20ServX.%20Your%20one%20time%20password%20is%20#{otp}."
    Net::HTTP.get(URI("http://trans.kapsystem.com/api/v3/index.php?method=sms&api_key=A261352e7111244296e599e1b25b3fd7d&to=#{number}&sender=ISERVX&message=#{msg}&format=json&custom=1,2&flash=0"))
  end

  # sets temp_otp to null after usage.
  def delete_temp_otp
    update_attribute(:temp_otp, nil)
  end

  # sets temp_otp to null after usage.
  def delete_otp
    update_attribute(:otp, nil)
  end

  def check_address(address)
    if address.blank? || address.length<4 || address.nil?
      self.errors.add(:address,"address can not be blank or less than 4 characters.")
      return false
    elsif self.address!=address 
      self.update_attribute(:address, address)
    end
    return true
  end

  def welcome_me
    UserMailer.welcome_email(self).deliver_now
  end
  
  def set_password
    password=Random.rand(100000..999999).to_s
    self.password=password     
  end

  def update_multiple_values
    update_attributes(temp_otp: nil)
  end

  def update_multiple_values1
    token= User.new_token
    user=User.find_by(remember_digest: token)
    while user do
      token= User.new_token
      user=User.find_by(remember_digest: token)      
    end    
    update_attributes(otp: nil,remember_digest: token)
  end

  def update_last_active_and_remember_token
    token= User.new_token
    user=User.find_by(remember_digest: token)
    while user do
      token= User.new_token
      user=User.find_by(remember_digest: token)      
    end    
    update_attributes(last_active: Time.zone.now,remember_digest: token)
  end 

  def update_last_active_gcm_token_and_remember_token(gcm_token)
    token= User.new_token
    user=User.find_by(remember_digest: token)
    while user do
      token= User.new_token
      user=User.find_by(remember_digest: token)      
    end   
    update_attributes(last_active: Time.zone.now,remember_digest: token, gcm_token: gcm_token)
  end   

  def update_last_active_apns_token_and_remember_token(apns_token)
    token= User.new_token
    user=User.find_by(remember_digest: token)
    while user do
      token= User.new_token
      user=User.find_by(remember_digest: token)      
    end     
    update_attributes(last_active: Time.zone.now,remember_digest: token, apns_token: apns_token)
  end   

  def many_updates
    token= User.new_token
    user=User.find_by(remember_digest: token)
    while user do
      token= User.new_token
      user=User.find_by(remember_digest: token)      
    end    
    update_attributes(otp: nil, remember_digest: token, last_active: Time.zone.now, activated: true, activated_at: Time.zone.now)
  end    

  def check_activated
    if self.activated==false
      update_columns(activated: true, activated_at: Time.zone.now)
    end
  end

  def all_bookings
    all_bookings=Array.new
    self.bookings.each do |booking|
      if !booking.package_id.nil?
        booking.get_package=booking.package.slice('name','cost')
      end
      booking=booking.slice('ref_code','jobs','status', 'pickup_date', 'slot', 'address', 'get_package', 'updated_at')
      all_bookings.push(booking)
    end
    self.wash_bookings.each do |wash_booking|
      if !wash_booking.wash_package_id.nil?
        wash_booking.get_package=wash_booking.wash_package.slice('name','cost', 'jobs','model')
      end      
      all_bookings << wash_booking.slice('ref_code','pickup_date', 'slot', 'address', 'updated_at', 'get_package')
    end
     all_bookings.sort_by! { |obj| obj[:updated_at] }
     all_bookings.reverse!
    return all_bookings
  end

    private

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end
end
