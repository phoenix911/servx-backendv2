class Api::V1::LocationsController < Api::V1::ApiController

    def index
    @locations = Location.order(:name)
    if !@locations.empty?   
      render json:{ code: 1,response: { msg: "Listing all locations.", locations: @locations.as_json(only: ['name','id','latitude','longitude'])}}
    else
      render json:{ code: 0,response:{ msg:"No locations available." }}
    end
  end

end
