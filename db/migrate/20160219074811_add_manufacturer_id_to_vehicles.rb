class AddManufacturerIdToVehicles < ActiveRecord::Migration
  def change
    add_reference :vehicles, :manufacturer, index: true, foreign_key: true
  end
end
