class AddAllToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :all, :boolean, default: false
  end
end
