require 'test_helper'

class DealershipvendorsControllerTest < ActionController::TestCase
  setup do
    @dealershipvendor = dealershipvendors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dealershipvendors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dealershipvendor" do
    assert_difference('Dealershipvendor.count') do
      post :create, dealershipvendor: { dealership_id: @dealershipvendor.dealership_id, vendor_id: @dealershipvendor.vendor_id }
    end

    assert_redirected_to dealershipvendor_path(assigns(:dealershipvendor))
  end

  test "should show dealershipvendor" do
    get :show, id: @dealershipvendor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dealershipvendor
    assert_response :success
  end

  test "should update dealershipvendor" do
    patch :update, id: @dealershipvendor, dealershipvendor: { dealership_id: @dealershipvendor.dealership_id, vendor_id: @dealershipvendor.vendor_id }
    assert_redirected_to dealershipvendor_path(assigns(:dealershipvendor))
  end

  test "should destroy dealershipvendor" do
    assert_difference('Dealershipvendor.count', -1) do
      delete :destroy, id: @dealershipvendor
    end

    assert_redirected_to dealershipvendors_path
  end
end
