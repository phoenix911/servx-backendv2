# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160530125016) do

  create_table "addresses", force: :cascade do |t|
    t.string   "label"
    t.text     "text"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "addresses", ["user_id"], name: "index_addresses_on_user_id"

  create_table "admins", force: :cascade do |t|
    t.string   "name"
    t.string   "mobile"
    t.string   "email"
    t.string   "password_digest"
    t.string   "image",           default: "default.png"
    t.string   "remember_digest"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true
  add_index "admins", ["mobile"], name: "index_admins_on_mobile", unique: true

  create_table "bookings", force: :cascade do |t|
    t.integer  "vehicle_id"
    t.datetime "drop_time"
    t.boolean  "urgent",        default: false
    t.string   "ref_code"
    t.text     "feedback"
    t.text     "jobs"
    t.integer  "status",        default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "user_id"
    t.integer  "dealership_id"
    t.integer  "offer_id"
    t.integer  "pickup_driver"
    t.integer  "drop_driver"
    t.float    "cost"
    t.integer  "package_id"
    t.text     "address"
    t.date     "pickup_date"
    t.string   "slot"
  end

  add_index "bookings", ["dealership_id"], name: "index_bookings_on_dealership_id"
  add_index "bookings", ["offer_id"], name: "index_bookings_on_offer_id"
  add_index "bookings", ["package_id"], name: "index_bookings_on_package_id"
  add_index "bookings", ["ref_code"], name: "index_bookings_on_ref_code", unique: true
  add_index "bookings", ["user_id"], name: "index_bookings_on_user_id"
  add_index "bookings", ["vehicle_id"], name: "index_bookings_on_vehicle_id"

  create_table "dealerships", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.string   "image"
    t.string   "gm_name"
    t.string   "gm_email"
    t.string   "gm_phone"
    t.string   "wm_name"
    t.string   "wm_email"
    t.string   "wm_phone"
    t.string   "ccm_name"
    t.string   "ccm_email"
    t.string   "ccm_phone"
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "activated",       default: false
    t.string   "email"
    t.integer  "manufacturer_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "dealerships", ["manufacturer_id"], name: "index_dealerships_on_manufacturer_id"

  create_table "dealershipvendors", force: :cascade do |t|
    t.integer  "vendor_id"
    t.integer  "dealership_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "dealershipvendors", ["dealership_id"], name: "index_dealershipvendors_on_dealership_id"
  add_index "dealershipvendors", ["vendor_id", "dealership_id"], name: "index_dealershipvendors_on_vendor_id_and_dealership_id", unique: true
  add_index "dealershipvendors", ["vendor_id"], name: "index_dealershipvendors_on_vendor_id"

  create_table "drivers", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "mobile"
    t.boolean  "deactivated",     default: false
    t.integer  "vendor_id"
    t.integer  "sub_vendor_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "image",           default: "default.png"
    t.integer  "dealership_id"
    t.boolean  "available",       default: true
  end

  add_index "drivers", ["dealership_id"], name: "index_drivers_on_dealership_id"
  add_index "drivers", ["email"], name: "index_drivers_on_email", unique: true
  add_index "drivers", ["mobile"], name: "index_drivers_on_mobile", unique: true
  add_index "drivers", ["sub_vendor_id"], name: "index_drivers_on_sub_vendor_id"
  add_index "drivers", ["vendor_id"], name: "index_drivers_on_vendor_id"

  create_table "getdealerships", force: :cascade do |t|
    t.integer  "dealership_id"
    t.integer  "location_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "getdealerships", ["dealership_id"], name: "index_getdealerships_on_dealership_id"
  add_index "getdealerships", ["location_id"], name: "index_getdealerships_on_location_id"

  create_table "getoffers", force: :cascade do |t|
    t.integer  "offer_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "dealership_id"
  end

  add_index "getoffers", ["dealership_id"], name: "index_getoffers_on_dealership_id"
  add_index "getoffers", ["offer_id"], name: "index_getoffers_on_offer_id"

  create_table "job_packages", force: :cascade do |t|
    t.integer  "job_id"
    t.integer  "package_id"
    t.integer  "quantity",   default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "job_packages", ["job_id"], name: "index_job_packages_on_job_id"
  add_index "job_packages", ["package_id"], name: "index_job_packages_on_package_id"

  create_table "jobs", force: :cascade do |t|
    t.string   "name"
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "manufacturers", force: :cascade do |t|
    t.string   "manufacturer"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.string   "text"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id"

  create_table "offers", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.datetime "expires_at"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "all",         default: false
    t.text     "description"
    t.text     "terms"
    t.boolean  "deactivated", default: false
    t.boolean  "wash",        default: false
  end

  create_table "package_vehicle_models", force: :cascade do |t|
    t.integer  "vehiclemodel_id"
    t.integer  "package_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "package_vehicle_models", ["package_id"], name: "index_package_vehicle_models_on_package_id"
  add_index "package_vehicle_models", ["vehiclemodel_id"], name: "index_package_vehicle_models_on_vehiclemodel_id"

  create_table "packages", force: :cascade do |t|
    t.string   "name"
    t.integer  "cost"
    t.boolean  "all",        default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "prices", force: :cascade do |t|
    t.integer  "vehiclemodel_id"
    t.integer  "job_id"
    t.float    "price"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "prices", ["job_id"], name: "index_prices_on_job_id"
  add_index "prices", ["vehiclemodel_id"], name: "index_prices_on_vehiclemodel_id"

  create_table "reviews", force: :cascade do |t|
    t.text     "review"
    t.integer  "rating"
    t.integer  "user_id"
    t.integer  "booking_id"
    t.integer  "dealership_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "reviews", ["booking_id"], name: "index_reviews_on_booking_id"
  add_index "reviews", ["dealership_id"], name: "index_reviews_on_dealership_id"
  add_index "reviews", ["user_id"], name: "index_reviews_on_user_id"

  create_table "service_advisors", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "mobile"
    t.boolean  "deactivated",     default: false
    t.integer  "vendor_id"
    t.integer  "sub_vendor_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "image",           default: "default.png"
    t.integer  "dealership_id"
  end

  add_index "service_advisors", ["dealership_id"], name: "index_service_advisors_on_dealership_id"
  add_index "service_advisors", ["email"], name: "index_service_advisors_on_email", unique: true
  add_index "service_advisors", ["mobile"], name: "index_service_advisors_on_mobile", unique: true
  add_index "service_advisors", ["sub_vendor_id"], name: "index_service_advisors_on_sub_vendor_id"
  add_index "service_advisors", ["vendor_id"], name: "index_service_advisors_on_vendor_id"

  create_table "sub_vendors", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "mobile"
    t.boolean  "deactivated",     default: false
    t.integer  "vendor_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "image",           default: "default.png"
    t.integer  "dealership_id"
    t.datetime "last_active"
  end

  add_index "sub_vendors", ["dealership_id"], name: "index_sub_vendors_on_dealership_id"
  add_index "sub_vendors", ["email"], name: "index_sub_vendors_on_email", unique: true
  add_index "sub_vendors", ["mobile"], name: "index_sub_vendors_on_mobile", unique: true
  add_index "sub_vendors", ["vendor_id"], name: "index_sub_vendors_on_vendor_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "mobile"
    t.integer  "otp"
    t.integer  "temp_otp"
    t.string   "password_digest"
    t.string   "gcm_token"
    t.datetime "last_active"
    t.datetime "activated_at"
    t.string   "remember_digest"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "activated",       default: false
    t.string   "apns_token"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["mobile"], name: "index_users_on_mobile", unique: true
  add_index "users", ["remember_digest"], name: "index_users_on_remember_digest", unique: true

  create_table "vehiclemodels", force: :cascade do |t|
    t.string   "name"
    t.integer  "manufacturer_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "image"
  end

  add_index "vehiclemodels", ["manufacturer_id"], name: "index_vehiclemodels_on_manufacturer_id"

  create_table "vehicles", force: :cascade do |t|
    t.string   "reg_num"
    t.integer  "user_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "deactivated",     default: false
    t.integer  "vehiclemodel_id"
  end

  add_index "vehicles", ["user_id"], name: "index_vehicles_on_user_id"
  add_index "vehicles", ["vehiclemodel_id"], name: "index_vehicles_on_vehiclemodel_id"

  create_table "vendors", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "mobile"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "image",           default: "default.png"
    t.datetime "last_active"
  end

  add_index "vendors", ["email"], name: "index_vendors_on_email", unique: true
  add_index "vendors", ["mobile"], name: "index_vendors_on_mobile", unique: true

  create_table "wash_bookings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "wash_package_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "ref_code"
    t.text     "address"
    t.date     "pickup_date"
    t.string   "slot"
  end

  add_index "wash_bookings", ["user_id"], name: "index_wash_bookings_on_user_id"
  add_index "wash_bookings", ["wash_package_id"], name: "index_wash_bookings_on_wash_package_id"

  create_table "wash_packages", force: :cascade do |t|
    t.string   "model"
    t.text     "jobs"
    t.integer  "cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
  end

end
