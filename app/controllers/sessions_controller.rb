class SessionsController < ApplicationController
  
  def create
    user = User.find_by(mobile: session_params[:mobile])
    if user && user.authenticate(session_params[:password])
      if user.activated?
        user.last_active_update
        if session_params[:gcm_token]
          user.update_gcm_token(session_params[:gcm_token])
        elsif session_params[:apns_token]
          user.update_apns_token(session_params[:apns_token])
        end 
        remember(user)
      	respond_to do |format|
          if user.address==nil
            user.address=""
          end
        	format.json { render json: {code: 1, msg: "Successfully logged in!", user: user.as_json(only: ['name','id','remember_digest','mobile','email',"address"])}}
      	end
      else
    		respond_to do |format|
    			format.json { render json: {code: 3, msg: "Account not activated. You will recieve an OTP shortly to activate your account.", user: user.as_json(only: ['mobile'])}}  
   			end
        user.send_otp_from_controller         
      end
    else
      if user
        respond_to do |format|
    			format.json { render json: {code: 0, msg: "Invalid password."}}
   			end
      else
        respond_to do |format|
    			format.json { render json: {code: 0, msg: "Invalid mobile/password combination."}}
   			end
      end
    end 
  end

  def destroy
  	user = User.find_by(id: session_params[:user_id])
    respond_to do |format|    
      if user
        if user.remember_digest==session_params[:remember_digest] 
            forget(user)
            format.json { render json: {code: 1, msg: "Successfully logged out."}} 
        else
          format.json { render json: {code: 2, msg: "Invalid access."}} 
        end          
      else
        format.json { render json: {code: 0, msg: "Account does not exist."}}  
      end
    end  
  end  

  def facebook
    access_token=session_params[:access_token]
    response=Net::HTTP.get(URI("https://graph.facebook.com/me?fields=email,name&access_token=#{access_token}"))
    responseJson=JSON.parse response
    respond_to do |format| 
      if responseJson["error"]
        format.json { render json: {code: 0, msg: "Invalid access. Please try logging in again from Facebook or signup."}} 
      elsif responseJson["email"]==nil
        format.json { render json: {code: 0, msg: "Please allow email while logging in through Facebook.", email_missing: 1}}
      elsif responseJson["email"]
        email=responseJson["email"]
        user=User.find_by(email: email)
        if user
          if user.activated?
            remember user
            user.last_active_update
            if user.address==nil
              user.address=""
            end            
            format.json { render json: {code: 1, msg: "Successfully logged in!", user: user.as_json(only: ['name','id','remember_digest','mobile','email','address']), already_exists: 1}}
          else
            format.json { render json: {code: 3, msg: "Account not activated. You will recieve an OTP shortly to activate your account.", user: user.as_json(only: ['mobile'])}}
            user.send_otp_from_controller            
          end         
        else
          format.json { render json: {code: 1, already_exists: 0, email: responseJson["email"] , name: responseJson["name"], msg: "Please enter your mobile number."}}           
        end
      end
    end
  end

  def google
    access_token=session_params[:access_token]
    response=Net::HTTP.get(URI("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=#{access_token}"))
    responseJson=JSON.parse response
    respond_to do |format| 
      if responseJson["error_description"]
        format.json { render json: {code: 0, msg: "Invalid access. Please try logging in again from Google or signup."}} 
      elsif responseJson["email"]==nil
        format.json { render json: {code: 0, msg: "Email not found. Please sign up.", email_missing: 1}}
      elsif responseJson["email"]
        email=responseJson["email"]
        user=User.find_by(email: email)
        if user
          if user.activated?
            remember user
            user.last_active_update
            if user.address==nil
              user.address=""
            end            
            format.json { render json: {code: 1, msg: "Successfully logged in!", user: user.as_json(only: ['name','id','remember_digest','mobile','email','address']), already_exists: 1}}
          else
            format.json { render json: {code: 3, msg: "Account not activated. You will recieve an OTP shortly to activate your account.", user: user.as_json(only: ['mobile'])}}
            user.send_otp_from_controller            
          end         
        else
          format.json { render json: {code: 1, already_exists: 0, email: responseJson["email"] , name: responseJson["name"], msg: "Please enter your mobile number."}}           
        end
      end
    end
  end  

  def oauth_mobile
    @user = User.new(session_params.except(:password, :remember_digest, :access_token, :user_id))
    @user.set_password
    if @user.save
      respond_to do |format|
        format.json { render json: {code: 1, hash: 0,msg: "You will receive your OTP shortly."}}
      end
    else
      respond_to do |format|
          format.json { render json: {code: 0, hash: 1, msg: @user.errors.messages}}
      end
    end  
  end
    
   private 

    def session_params
      params.permit(:mobile, :password, :access_token, :user_id, :remember_digest, :email, :name, :gcm_token, :apns_token, :with_vehicle)
    end     
end
