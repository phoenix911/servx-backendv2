class Api::V1::SessionsController < Api::V1::ApiController
  
  def create
    user = User.find_by(mobile: session_params[:mobile])
    if user && user.authenticate(session_params[:password])
      if user.activated?
        if session_params[:gcm_token]
          user.update_last_active_gcm_token_and_remember_token(session_params[:gcm_token])
        elsif session_params[:apns_token]
          user.update_last_active_apns_token_and_remember_token(session_params[:apns_token])
        else
          user.update_last_active_and_remember_token          
        end 
        if session_params[:with_vehicle] == 1 || session_params[:with_vehicle] == "1"
        	render json: {code: 1, response: { msg: "Successfully logged in!", user: user.as_json(include: {active_vehicles: {methods:[:image_and_model,:image,:model, :manufacturer_id],only: ['id', 'reg_num', 'vehiclemodel_id']}, addresses:{only: [ 'text', 'label']}},only: ['name','remember_digest','mobile','email'])}}
        else
          render json: {code: 1, response: { msg: "Successfully logged in!", user: user.as_json(include:{addresses: {only: [ 'text', 'label']}},only: ['name','remember_digest','mobile','email',"address"])}}
        end          
      else
    		render json: {code: 3, response: { msg: "Account not activated. You will recieve an OTP shortly to activate your account.", user: user.as_json(only: ['mobile'])}}
        user.send_otp_from_controller         
      end
    else
      if user
    		render json: {code:0,response:{ msg:"Invalid password."}}
      else
    		render json: {code: 0,response:{ msg:"Invalid mobile/password combination."}}
      end
    end 
  end 

  def facebook
    access_token=session_params[:access_token]
    response_facebook=Net::HTTP.get(URI("https://graph.facebook.com/me?fields=email,name&access_token=#{access_token}"))
    responseJson=JSON.parse response_facebook
    if responseJson["error"]     
      render json: {code:0,response:{msg: "Invalid access. Please try logging in again from Facebook or signup."}}
    elsif responseJson["email"]==nil
      render json: {code:0,response:{ msg: "Please allow email while logging in through Facebook.", email_missing: 1}}
    elsif responseJson["email"]
      email=responseJson["email"]
      user=User.find_by(email: email)
      if user
        if user.activated?
          if session_params[:gcm_token]
            user.update_last_active_gcm_token_and_remember_token(session_params[:gcm_token])
          elsif session_params[:apns_token]
            user.update_last_active_apns_token_and_remember_token(session_params[:apns_token])
          else
            user.update_last_active_and_remember_token          
          end         
          if session_params[:with_vehicle] == 1 || session_params[:with_vehicle] == "1"
            render json: {code: 1, response: { msg: "Successfully logged in!", user: user.as_json(include: {active_vehicles: {methods:[:image_and_model,:image,:model, :manufacturer_id],only: ['id', 'reg_num', 'vehiclemodel_id']}, addresses:{only: ['text', 'label']}},only: ['name','remember_digest','mobile','email']),already_exists: 1}}
          else
            render json: {code: 1, response: { msg: "Successfully logged in!", user: user.as_json(include:{addresses: {only: ['text', 'label']}},only: ['name','remember_digest','mobile','email']),already_exists: 1}}
          end 
        else
          render json: {code: 3,  response:{msg: "Account not activated. You will receive an OTP shortly to activate your account.", user: user.as_json(only: ['mobile'])}}
          user.send_otp_from_controller
        end         
      else    
        render json: {code: 1,response:{ already_exists: 0, email: responseJson["email"] , name: responseJson["name"], msg: "Please enter your mobile number."}}           
      end
    end
  end

  def google
    access_token=session_params[:access_token]
    response_google=Net::HTTP.get(URI("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=#{access_token}"))
    responseJson=JSON.parse response_google
    if responseJson["error_description"]
      render json: {code: 0,response:{ msg:"Invalid access. Please try logging in again from Facebook or signup."}}
    elsif responseJson["email"]==nil
      render json: {code:0,response: { msg:"Please allow email while logging in through Facebook.", email_missing: 1}}
    elsif responseJson["email"]
      email=responseJson["email"]
      user=User.find_by(email: email)
      if user
        if user.activated?
          if session_params[:gcm_token]
            user.update_last_active_gcm_token_and_remember_token(session_params[:gcm_token])
          elsif session_params[:apns_token]
            user.update_last_active_apns_token_and_remember_token(session_params[:apns_token])
          else
            user.update_last_active_and_remember_token          
          end
          if session_params[:with_vehicle] == 1 || session_params[:with_vehicle] == "1"
            render json: {code: 1, response: { msg: "Successfully logged in!", user: user.as_json(include: {active_vehicles: {methods:[:image_and_model,:image,:model, :manufacturer_id],only: ['id', 'reg_num', 'vehiclemodel_id']}, addresses:{only: ['text', 'label']}},only: ['name','remember_digest','mobile','email']),already_exists: 1}}
          else
            render json: {code: 1, response: { msg: "Successfully logged in!", user: user.as_json(include:{addresses: {only: ['text', 'label']}},only: ['name','id','remember_digest','mobile','email']) ,already_exists: 1}}
          end 
        else
          user.send_otp_from_controller          
          render json: { code: 3,response:{ msg: "Account not activated. You will receive an OTP shortly to activate your account.", user: user.as_json(only: ['mobile'])}}
        end         
      else
        render json: {code: 1,response:{ already_exists: 0, email: responseJson["email"] , name: responseJson["name"], msg: "Please enter your mobile number."}}           
      end
    end
  end    

  def oauth_mobile
    @user = User.new(session_params.except(:password, :remember_digest, :access_token, :user_id, :with_vehicle))
    @user.set_password
    if @user.save      
      render json: {code:1,response:{ msg: "You will receive your OTP shortly."}}
    else     
      render json: {code: 0, response:{msg: @user.errors.messages}}
    end  
  end

  def form
    AdminMailer.form(session_params[:name],session_params[:mobile],session_params[:msg]).deliver_now
    msg= "#{session_params[:name]} with mobile number #{session_params[:mobile]} sent you a message on the website, please check your email."
    res=Net::HTTP.get(URI("http://trans.kapsystem.com/api/v3/index.php?method=sms&api_key=A261352e7111244296e599e1b25b3fd7d&to=9999666563&sender=ISERVX&message=#{msg}&format=json&custom=1,2&flash=0"))    
    render json: { code: 1,response:{ msg: "message sent."}}
    
  end
    
   private 

    def session_params
      params.permit(:mobile, :password, :access_token, :user_id, :remember_digest, :email, :name, :gcm_token, :apns_token, :with_vehicle, :msg)
    end     
end
