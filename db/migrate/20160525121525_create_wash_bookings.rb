class CreateWashBookings < ActiveRecord::Migration
  def change
    create_table :wash_bookings do |t|
      t.references :user, index: true, foreign_key: true
      t.references :wash_package, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
