class CreateVendors < ActiveRecord::Migration
  def change
    create_table :vendors do |t|
      t.string :name
      t.string :email
      t.string :mobile
      t.string :password_digest
      t.string :remember_digest
      t.references :dealership, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
