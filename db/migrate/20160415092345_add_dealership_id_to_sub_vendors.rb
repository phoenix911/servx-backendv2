class AddDealershipIdToSubVendors < ActiveRecord::Migration
  def change
    add_reference :sub_vendors, :dealership, index: true, foreign_key: true
  end
end
