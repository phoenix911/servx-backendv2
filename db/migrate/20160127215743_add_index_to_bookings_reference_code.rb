class AddIndexToBookingsReferenceCode < ActiveRecord::Migration
  def change
  	add_index :bookings, :reference_code, unique: true
  end
end
