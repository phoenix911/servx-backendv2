class AddDealershipIdToServiceAdvisors < ActiveRecord::Migration
  def change
    add_reference :service_advisors, :dealership, index: true, foreign_key: true
  end
end
