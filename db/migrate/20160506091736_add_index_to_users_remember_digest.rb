class AddIndexToUsersRememberDigest < ActiveRecord::Migration
  def change
  	add_index :users, :remember_digest, unique: true
  end
end
