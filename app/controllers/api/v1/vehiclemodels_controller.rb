class Api::V1::VehiclemodelsController < Api::V1::ApiController
  before_action :set_vehiclemodel, only: [:get_packages, :get_jobs]

  def get_packages
  	@packages=@vehiclemodel.packages
    if !@packages.empty?  
      render json:{ code: 1, response: {msg: "Listing all packages for this model.", packages: @packages.as_json(include:{job_packages:{methods: [:get_job], only: ['quantity']}},only: ['name', 'id', 'cost'])}}
    else
      render json:{ code: 0,response:{ msg: "No packages available." }}
    end  	
  end

  def get_jobs
    @prices=@vehiclemodel.prices
    if !@prices.empty?  
      render json:{ code: 1, response: {msg: "Listing all jobs for this model.", jobs: @prices.as_json(methods: [:get_job], only: ['price'])}}
    else
      render json:{ code: 0,response:{ msg: "No jobs present." }}
    end   
  end  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehiclemodel
      @vehiclemodel = Vehiclemodel.find_by(name: params[:name])
      if @vehiclemodel.nil?
        render json:{ code: 0,response:{ msg:"Model not found."}}
      end        
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vehiclemodel_params
      params.permit(:name, :manufacturer_id)
    end
 end
