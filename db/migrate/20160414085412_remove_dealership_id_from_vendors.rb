class RemoveDealershipIdFromVendors < ActiveRecord::Migration
  def change
  	remove_column :vendors, :dealership_id
  end
end
