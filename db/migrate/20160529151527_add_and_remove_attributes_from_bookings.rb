class AddAndRemoveAttributesFromBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :address, :text
    add_column :bookings, :pickup_date, :date
    add_column :bookings, :slot, :integer, default: 0
    remove_column :bookings, :address_id
    remove_column :bookings, :pickup_time  	
  end
end
