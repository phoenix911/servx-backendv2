class CreateJobPackages < ActiveRecord::Migration
  def change
    create_table :job_packages do |t|
      t.references :job, index: true, foreign_key: true
      t.references :package, index: true, foreign_key: true
      t.integer :quantity, default: 1
      
      t.timestamps null: false
    end
  end
end
