class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token
  include SessionsHelper

  private
    # checks remember_digest for authentication.
    def logged_in_user
      @current_user = User.find_by(id: params[:user_id])
      if @current_user && @current_user.remember_digest == params[:remember_digest]
        @user = @current_user
      elsif @current_user && @current_user.remember_digest != params[:remember_digest]
        respond_to do |format|
          format.json { render json:{ code: 2, msg: "Please sign in." }}
        end
      else
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "User not found." }}
        end                
      end
    end

    # checks remember_digest for authentication.
    def logged_in_vendor
      @current_vendor = Vendor.find_by(id: params[:vendor_id])
      if @current_vendor && @current_vendor.remember_digest == params[:remember_digest]
        @vendor = @current_vendor
      elsif @current_vendor && @current_vendor.remember_digest != params[:remember_digest]
        respond_to do |format|
          format.json { render json:{ code: 2, msg: "Please sign in." }}
        end
      else
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Admin not found." }}
        end                
      end
    end    

    # checks remember_digest for authentication.
    def logged_in_sub_vendor
      @current_sub_vendor = SubVendor.find_by(id: params[:sub_vendor_id])
      if @current_sub_vendor && @current_sub_vendor.remember_digest == params[:remember_digest]
        @sub_vendor = @current_sub_vendor
      elsif @current_sub_vendor && @current_sub_vendor.remember_digest != params[:remember_digest]
        respond_to do |format|
          format.json { render json:{ code: 2, msg: "Please sign in." }}
        end
      else
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Admin not found." }}
        end                
      end
    end


    # checks remember_digest for authentication.
    def logged_in_service_advisor
      @current_service_advisor = ServiceAdvisor.find_by(id: params[:service_advisor_id])
      if @current_service_advisor && @current_service_advisor.remember_digest == params[:remember_digest]
        @service_advisor = @service_advisor_vendor
      elsif @current_service_advisor && @current_service_advisor.remember_digest != params[:remember_digest]
        respond_to do |format|
          format.json { render json:{ code: 2, msg: "Please sign in." }}
        end
      else
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Service Advisor not found." }}
        end                
      end
    end 

   # checks remember_digest for authentication.
    def logged_in_driver
      @current_driver = Driver.find_by(id: params[:driver_id])
      if @current_driver && @current_driver.remember_digest == params[:remember_digest]
        @driver = @current_driver
      elsif @current_driver && @current_driver.remember_digest != params[:remember_digest]
        respond_to do |format|
          format.json { render json:{ code: 2, msg: "Please sign in." }}
        end
      else
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Driver not found." }}
        end                
      end
    end 


   # checks remember_digest for authentication.
    def logged_in_admin
      @current_admin = Admin.find_by(id: params[:admin_id])
      if @current_admin && @current_admin.remember_digest == params[:remember_digest]
        @admin = @current_admin
      elsif @current_admin && @current_admin.remember_digest != params[:remember_digest]
        respond_to do |format|
          format.json { render json:{ code: 2, msg: "Please sign in." }}
        end
      else
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Admin not found." }}
        end                
      end
    end 

    def write(request)
      target  = "#{Rails.public_path}/text.txt"
      content = request+" "+Time.zone.now.to_s + "\n"
      File.open(target, "a") do |f|
        f.write(content)
      end
    end       

end
