json.array!(@reviews) do |review|
  json.extract! review, :id, :review, :rating, :user_id, :booking_id, :dealership_id
  json.url review_url(review, format: :json)
end
