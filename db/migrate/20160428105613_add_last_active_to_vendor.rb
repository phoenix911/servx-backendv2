class AddLastActiveToVendor < ActiveRecord::Migration
  def change
    add_column :vendors, :last_active, :datetime
  end
end
