class AddDeactivatedToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :deactivated, :boolean, default: false
  end
end
