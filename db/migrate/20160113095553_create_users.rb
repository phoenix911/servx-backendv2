class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :mobile
      t.text :address
      t.integer :otp
      t.integer :temp_otp
      t.string :password_digest
      t.string :gcm_token
      t.datetime :last_active
      t.datetime :activated_at
      t.string :remember_digest

      t.timestamps null: false
    end
  end
end
