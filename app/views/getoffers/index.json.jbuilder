json.array!(@getoffers) do |getoffer|
  json.extract! getoffer, :id, :manufacturer_id, :offer_id
  json.url getoffer_url(getoffer, format: :json)
end
