require 'test_helper'

class VehiclemodelsControllerTest < ActionController::TestCase
  setup do
    @vehiclemodel = vehiclemodels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vehiclemodels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vehiclemodel" do
    assert_difference('Vehiclemodel.count') do
      post :create, vehiclemodel: { manufacturer_id: @vehiclemodel.manufacturer_id, name: @vehiclemodel.name }
    end

    assert_redirected_to vehiclemodel_path(assigns(:vehiclemodel))
  end

  test "should show vehiclemodel" do
    get :show, id: @vehiclemodel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vehiclemodel
    assert_response :success
  end

  test "should update vehiclemodel" do
    patch :update, id: @vehiclemodel, vehiclemodel: { manufacturer_id: @vehiclemodel.manufacturer_id, name: @vehiclemodel.name }
    assert_redirected_to vehiclemodel_path(assigns(:vehiclemodel))
  end

  test "should destroy vehiclemodel" do
    assert_difference('Vehiclemodel.count', -1) do
      delete :destroy, id: @vehiclemodel
    end

    assert_redirected_to vehiclemodels_path
  end
end
