require 'test_helper'

class GetoffersControllerTest < ActionController::TestCase
  setup do
    @getoffer = getoffers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:getoffers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create getoffer" do
    assert_difference('Getoffer.count') do
      post :create, getoffer: { manufacturer_id: @getoffer.manufacturer_id, offer_id: @getoffer.offer_id }
    end

    assert_redirected_to getoffer_path(assigns(:getoffer))
  end

  test "should show getoffer" do
    get :show, id: @getoffer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @getoffer
    assert_response :success
  end

  test "should update getoffer" do
    patch :update, id: @getoffer, getoffer: { manufacturer_id: @getoffer.manufacturer_id, offer_id: @getoffer.offer_id }
    assert_redirected_to getoffer_path(assigns(:getoffer))
  end

  test "should destroy getoffer" do
    assert_difference('Getoffer.count', -1) do
      delete :destroy, id: @getoffer
    end

    assert_redirected_to getoffers_path
  end
end
