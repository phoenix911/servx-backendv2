json.array!(@service_advisors) do |service_advisor|
  json.extract! service_advisor, :id, :name, :email, :password_digest, :remember_digest, :mobile, :vendor_mobile
  json.url service_advisor_url(service_advisor, format: :json)
end
