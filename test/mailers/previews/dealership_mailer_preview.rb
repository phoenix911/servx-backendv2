# Preview all emails at http://localhost:3000/rails/mailers/dealership_mailer
class DealershipMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/dealership_mailer/dealership_booking_detail
  def dealership_booking_detail
    DealershipMailer.dealership_booking_detail
  end

end
