class AddDealershipIdToBookings < ActiveRecord::Migration
  def change
    add_reference :bookings, :dealership, index: true, foreign_key: true
  end
end
