json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :manufacturer, :model, :registration_number, :service, :user_id
  json.url vehicle_url(vehicle, format: :json)
end
