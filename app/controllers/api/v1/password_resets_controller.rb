class Api::V1::PasswordResetsController < Api::V1::ApiController
  
  # POST /password_resets
  # POST /password_resets.json
  def create
    @user = User.find_by(mobile: password_reset_params[:mobile])
    if @user
    	@user.send_otp_from_controller
    	render json:{code:1, response:{ msg:"You will receive your OTP shortly."}}
    else
      render json:{code:0, response:{ msg:"Account does not exist."}}
  	end
  end

  def update
    # checking this because during updating I have allowed nil value. Figure out why.
    if password_reset_params[:password].nil? || password_reset_params[:password].blank?
        render json:{ code: 0,response:{ msg:"Password can't be blank."}}
      return
    end
    @user = User.find_by(mobile: password_reset_params[:mobile])
    if !@user
      render json:{ code:0,response:{ msg:"User does not exist."}}
    elsif @user.otp==password_reset_params[:otp].to_i
       @user.check_activated              
      if @user.update_attributes(password_reset_params.except(:otp))
        @user.update_multiple_values1
        render json:{ code: 1,response:{ msg: "password updated.", user: @user.as_json(only: ['name','id','remember_digest','mobile','email','address'])}}
      else
        render json:{ code:0,response:{ msg:@user.errors.messages}}
      end
    else
      render json:{ code:0,response:{ msg:"OTP doesn't match."}}
    end        
	end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def password_reset_params
      params.permit(:mobile, :password, :otp)
    end
end
