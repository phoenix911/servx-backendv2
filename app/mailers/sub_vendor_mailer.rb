class SubVendorMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.sub_vendor_mailer.offer_mail.subject
  #
	def offer_mail(dealership,name,terms,description,expires_at)
		@dealership=dealership
		@name=name
		@terms=terms
		@description=description
		@expirers_at= expires_at
    require 'mailgun'
    # PUT API-KEY IN ENVIRONMENT VARIABLE FILE!!! keeping it open for development right now.
    # First, instantiate the Mailgun Client with your API key
    mg_client = Mailgun::Client.new "key-792d235a48f4513679ff561935d44bb5"

   html = render_to_string template: "vendor_mailer/offer_mail.html.erb"
    text= render_to_string template: "vendor_mailer/offer_mail.text.erb"
    # Define your message parameters
    message_params = {:from    => 'ServX <contact@servx.in>',  
                      :to      => 'saransh@servx.in',
                      #:cc      => 'shubham@servx.in, akansh@servx.in, gaurav@servx.in',
                      :subject => 'Offer made by vendor',
                      :text    => text.to_str,
                      :html    => html.to_str}

    # Send your message through the client
    mg_client.send_message "connx.in", message_params 		
	end
end
