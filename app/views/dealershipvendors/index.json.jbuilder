json.array!(@dealershipvendors) do |dealershipvendor|
  json.extract! dealershipvendor, :id, :vendor_id, :dealership_id
  json.url dealershipvendor_url(dealershipvendor, format: :json)
end
