json.array!(@packages) do |package|
  json.extract! package, :id, :name, :cost, :all
  json.url package_url(package, format: :json)
end
