class Address < ActiveRecord::Base
  belongs_to :user
  validates :text, presence: {message: "Address can't be blank."}, length: { minimum: 4, message: "Address is not valid (minimum 4 characters)." }
  validates :label, presence: {message: "label not present."}
end
