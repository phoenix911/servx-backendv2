class VendorsController < ApplicationController
  before_action :logged_in_vendor, only: [:signout, :set_droptime, :set_booking_status, :bookings, :dealerships, :dealership, :set_booking_status, 
                                          :assign_pickup_driver, :assign_drop_driver, :booking, :available_drivers, :unavailable_drivers, :drivers,
                                          :user_bookings, :driver_status, :update_booking, :driver_bookings, :make_offer, :vendor_manufacturers]
  before_action :check_booking,    only: [:set_droptime, :update_booking, :assign_pickup_driver, :assign_drop_driver, :booking]
  before_action :check_relation,   only: [:bookings, :dealership, :drivers, :available_drivers, :unavailable_drivers, :user_bookings]
  before_action :check_driver,     only: [:assign_pickup_driver, :assign_drop_driver, :driver_status, :driver_bookings]
  before_action :check_booking_compatibilty_with_driver, only: [:assign_pickup_driver, :assign_drop_driver]
  before_action :check_fields,     only: [:make_offer]
  before_action :check_offer_on,     only: [:make_offer]  
  before_action :check_status,     only: [:update_booking]  

  def dealerships
    @dealerships = @vendor.dealerships
    respond_to do |format|
      if @dealerships.empty?
        format.json { render json:{ code: 0, msg: "No Dealership(s) found." }}
      else
        format.json { render json:{ code: 1, msg: "Listing all dealerships.", dealerships: @dealerships.as_json(only: ['id', 'name', 'address', 'image']) }}
      end 
    end    
  end

  def dealership
    respond_to do |format|
      format.json { render json:{ code: 1, msg: "Listing all dealerships.", dealerships: @dealership.as_json(only: ['id', 'name', 'address', 'image']) }}
    end 
  end

  def bookings
    @vendor.last_active
    respond_to do |format|
      bookings = @dealership.bookings.order('id DESC')
      # below query fetches users too.use a for loop to get users. typical N+1 problem
      #bookings=Booking.includes(:user).where(dealership_id: vendor_params[:dealership_id])
      if bookings.nil?
        format.json { render json:{ code: 0, msg: "No bookings found.", bookings: bookings}}
      else
        format.json { render json:{ code: 1, msg: "Listing all bookings.", bookings: bookings.as_json(include: {user: {only: ['id', 'name','mobile','address']},
          vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}}, only: ['id', 'reference_code','jobs','pickup_time', 'status', 'drop_time']) }}
      end 
    end   
  end

  def booking
    @booking.change_job_format
    respond_to do |format|
      pickup_driver=Driver.find_by(id: @booking[:pickup_driver])
      drop_driver=Driver.find_by(id: @booking[:drop_driver])
      if pickup_driver && drop_driver
      format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
          vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
          pickup_driver: pickup_driver.as_json(only: ['id', 'name', 'mobile', 'image']), drop_driver: drop_driver.as_json(only: ['id', 'name', 'mobile', 'image'])}}
      elsif pickup_driver
        format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
          vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
          pickup_driver: pickup_driver.as_json(only: ['id', 'name', 'mobile', 'image'])}}
      elsif drop_driver
        format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
          vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}},only: ['reference_code','jobs','pickup_time', 'status', 'cost']),
          drop_driver: drop_driver.as_json(only: ['id', 'name', 'mobile', 'image'])}} 
      else
        format.json { render json:{ code: 1, booking: @booking.as_json(include: {user: {only: ['name','mobile',"address"]},dealership: {only: ['id','name','address','image']},
          vehicle: {except: ['created_at','updated_at','user_id','manufacturer_id','deactivated', 'id']}}, only: ['reference_code','jobs','pickup_time', 'status', 'cost'])}} 
      end                   
    end  
  end 

  # send push notification when drop time is set.
  def set_droptime
    respond_to do |format|
      @booking.update_drop_time(vendor_params[:drop_time])
      format.json { render json:{ code: 1, msg: "drop time is #{vendor_params[:drop_time]} for booking #{vendor_params[:reference_code]}" }}
    end 
  end

  def user_bookings
    respond_to do |format|
      user = User.find_by(id: vendor_params[:user_id])
      if user
        bookings = user.bookings.where(dealership_id: vendor_params[:dealership_id])
        if !bookings.empty?
          format.json { render json:{ code: 1, msg: "Listing all bookings of this user from this dealership.", bookings: bookings}}
        else
          format.json { render json:{ code: 0, msg: "No bookings of this user from this dealership found.", bookings: bookings}} 
        end
      else
        format.json { render json:{ code: 0, msg: "User not found."}} 
      end
    end    
  end

  def assign_pickup_driver
    respond_to do |format|
      if vendor_params[:driver_id]== "0" || vendor_params[:driver_id]== 0
        if !@booking[:pickup_driver].nil?
          driver=Driver.find_by(id: @booking[:pickup_driver])
          if driver
            driver.update_status(1)
          end
        end
        @booking.update_pickup_driver(nil)
        format.json { render json:{ code: 1, msg: "Pick up driver removed." }}        
      else 
        @booking.update_pickup_driver(vendor_params[:driver_id].to_i)
        @driver.update_status(0)
        format.json { render json:{ code: 1, msg: "Driver assigned to booking with reference code: #{@booking.reference_code} for pickup." }}
      end
    end
  end

  def assign_drop_driver
    respond_to do |format|
      if vendor_params[:driver_id]== "0" || vendor_params[:driver_id]== 0
        if !@booking[:drop_driver].nil?
          driver=Driver.find_by(id: @booking[:drop_driver])
          if driver
            driver.update_status(1)
          end
        end
        @booking.update_drop_driver(nil)
        format.json { render json:{ code: 1, msg: " Drop driver removed." }}        
      else 
        @booking.update_drop_driver(vendor_params[:driver_id].to_i)
        @driver.update_status(0)
        format.json { render json:{ code: 1, msg: "Driver assigned to booking with reference code: #{@booking.reference_code} for drop." }}
      end
    end
  end

  def update_booking
    respond_to do |format|
     vendor_params[:status]= Booking.statuses[vendor_params[:status]]
      if @booking.update_columns(vendor_params.slice(:status, :pickup_time, :jobs, :cost))
        if vendor_params[:status]=="Your vehicle has reached the service centre"
          if !@booking.pickup_driver.nil?
            driver=Driver.find_by(@booking.pickup_driver)
            if driver
              driver.update_status(1)
            end
          end 
          if driver
            format.json { render json:{ code: 1, msg: "updated booking. Updated #{driver.name}'s status with available", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}               
          else
            format.json { render json:{ code: 1, msg: "updated booking.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}               
          end
        elsif vendor_params[:status]=="Payment received"
          if !@booking.drop_driver.nil?
            driver=Driver.find_by(@booking.drop_driver)
            if driver
              driver.update_status(1)
            end
          end 
          if driver
            format.json { render json:{ code: 1, msg: "updated booking status. Updated #{driver.name}'s status with available", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}               
          else
            format.json { render json:{ code: 1, msg: "updated booking.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}                         
          end
        else
          format.json { render json:{ code: 1, msg: "updated booking.", booking: @booking.as_json(only: ['reference_code','jobs','pickup_time','status', 'cost'])}}  
        end
      else
        format.json { render json:{ code: 0, msg: @booking.errors.messages }}
      end  
    end 
  end

  def signin
    @vendor= Vendor.find_by(mobile: vendor_params[:mobile])
    respond_to do |format|
      if @vendor && @vendor.authenticate(vendor_params[:password])
        # @vendor.update_remember
        # @vendor.last_active
        @vendor.update_mutiple_values
        format.json { render json: {code: 1, msg: "Successfully logged in!", vendor: @vendor.as_json(only: ['name','id','remember_digest','mobile','email','image'])}}
      elsif @vendor
        format.json { render json: {code: 0, msg: "Invalid password"}}
      else
        format.json { render json: {code: 0, msg: "Invalid mobile/password combination."}}
      end
    end  
  end

  def signout
    @vendor.forget
    respond_to do |format|
     format.json { render json: {code: 1, msg: "Successfully logged out."}}       
    end
  end

  def drivers
    @drivers= @dealership.drivers.where(deactivated: :false)
    respond_to do |format|
      format.json { render json:{ code: 1, msg: "Listing all drivers of #{@dealership.name}.", drivers: @drivers.as_json(only: ['id', 'name', 'image', 'mobile', 'available']) }}
    end
  end

  def available_drivers
   # @drivers= @dealership.drivers.where(deactivated: :false,available: :true)
    @drivers= @dealership.drivers.where('deactivated = ? AND available = ?', :false, :true)
    respond_to do |format|
      format.json { render json:{ code: 1, msg: "Listing all available drivers of #{@dealership.name}.", drivers: @drivers.as_json(only: ['id', 'name', 'image', 'mobile', 'available']) }}
    end
  end

  def unavailable_drivers
   # @drivers= @dealership.drivers.where(deactivated: :false,available: :false)
    @drivers= @dealership.drivers.where('deactivated = ? AND available = ?', :false, :false)
    respond_to do |format|
      format.json { render json:{ code: 1, msg: "Listing all engaged drivers of #{@dealership.name}.", drivers: @drivers.as_json(only: ['id', 'name', 'image', 'mobile', 'available']) }}
    end
  end

  def driver_status
    respond_to do |format|
      if vendor_params[:available]
        if  vendor_params[:available].empty?
          format.json { render json: {code: 0, msg: "please select 1 or 0."}}          
        else 
          if vendor_params[:available]=="1" || vendor_params[:available]==1
            @driver.update_status(1)    
            format.json { render json: {code: 1, msg: "Successfully updated driver status with available."}}  
          elsif vendor_params[:available]=="0" || vendor_params[:available]==0
            @driver.update_status(0)     
            format.json { render json: {code: 1, msg: "Successfully updated driver status with unavailable."}}
          end
        end  
      else
        format.json { render json: {code: 0, msg: "please select 1 or 0."}}
      end
    end    
  end

  def driver_bookings
    bookings=Booking.where('pickup_driver = ? OR drop_driver = ?', vendor_params[:driver_id], vendor_params[:driver_id])
    respond_to do |format|
      if !bookings.empty?
        format.json { render json: {code: 1, msg: "Showing all bookings assigned to this driver.", bookings:bookings}}      
      else
        format.json { render json: {code: 1, msg: "No bookings found."}} 
      end
    end  
  end

  def make_offer
    respond_to do |format|
      if vendor_params[:manufacturer_id]
        manufacturer= Manufacturer.find_by(id: vendor_params[:manufacturer_id])
        if manufacturer
          dealerships=@vendor.dealerships.where(manufacturer_id: manufacturer[:id] )
          debugger
          VendorMailer.offer_mail_for_manufacturer(vendor_params[:offer_name],vendor_params[:terms],vendor_params[:description],vendor_params[:expires_at],dealerships).deliver_now
          format.json { render json: {code: 1, msg: "A mail has been sent to one of our executives. We will contact you shortly."}}
        else
          format.json { render json: {code: 1, msg: "Manufacturer not found."}}          
        end         
      else
        VendorMailer.offer_mail(vendor_params[:offer_name],vendor_params[:terms],vendor_params[:description],vendor_params[:expires_at],vendor_params[:array]).deliver_now
        format.json { render json: {code: 1, msg: "A mail has been sent to one of our executives. We will contact you shortly."}}   
      end       
    end   
  end

  # PATCH/PUT /vendors/1
  # PATCH/PUT /vendors/1.json
  #def update
  #   respond_to do |format|
  #     if @vendor.update(vendor_params)
  #       format.html { redirect_to @vendor, notice: 'Vendor was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @vendor }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @vendor.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # DELETE /vendors/1
  # # DELETE /vendors/1.json
  # def destroy
  #   @vendor.destroy
  #   respond_to do |format|
  #     format.html { redirect_to vendors_url, notice: 'Vendor was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  def vendor_manufacturers
    # Use this in make offer
    @manufacturers= @vendor.dealerships.includes(:manufacturer)
    respond_to do |format|
      if @manufacturers.empty?
        format.json { render json: {code: 0, msg: "No manufacturers found."}}
      else 
       @manufacturers= @vendor.get_manufacturers
        format.json { render json: {code: 1, msg: "Listing all manufacturers", manufacturers: @manufacturers.as_json(only: ['manufacturer','id'])}}         
      end 
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vendor
      @vendor = Vendor.find_by(params[:vendor_id])
      if @vendor.nil? 
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Vendor not found." }}
        end
      end      
    end

    def check_relation
      @dealership=Dealership.find_by(id: vendor_params[:dealership_id])
      if @dealership
        all_vendors = @dealership.vendors
        if !all_vendors.include?(@vendor)
          respond_to do |format|
            format.json { render json:{ code: 0, msg: "You can not access this dealership." }}
          end
        end
      else
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "dealership not found." }}
        end
      end 
    end

    def check_driver
      if vendor_params[:driver_id]!= "0"
        @driver=Driver.find_by(id: vendor_params[:driver_id])
        if @driver
          if !@driver.deactivated
            if !@driver.vendor_id.nil?
              @temp_vendor=Vendor.find_by(id: @driver.vendor_id)
              #no need to check but still checking
              if @temp_vendor
                dealerships=@vendor.dealerships
                temp_dealerships=@temp_vendor.dealerships
                if (temp_dealerships & dealerships).empty?
                  respond_to do |format|
                    format.json { render json:{ code: 0, msg: "This driver doesn't belong to this dealership." }}
                  end        
                end
              else
                respond_to do |format|
                  format.json { render json:{ code: 0, msg: "Admin not found." }}
                end            
              end
            elsif !@driver.sub_vendor_id.nil?
              @temp_sub_vendor=SubVendor.find_by(id: @driver.sub_vendor_id)
              if @temp_sub_vendor
                dealerships = @vendor.dealerships
                if dealerships.ids.include?(@temp_sub_vendor.dealership_id)
                  respond_to do |format|
                    format.json { render json:{ code: 0, msg: "Admin not found." }}
                  end        
                end          
              else
                respond_to do |format|
                  format.json { render json:{ code: 0, msg: "Admin not found." }}
                end 
              end      
            end
          else
            respond_to do |format|
              format.json { render json:{ code: 0, msg: "Driver not found." }}
            end            
          end  
        else
          respond_to do |format|
            format.json { render json:{ code: 0, msg: "Driver not found." }}
          end        
        end
      end  
    end

    def check_booking_compatibilty_with_driver
      if vendor_params[:driver_id]!= "0"   
        if @driver.dealership_id != @booking.dealership_id
          respond_to do |format|
            format.json { render json:{ code: 0, msg: "This driver does not belong to this dealership." }}
          end 
        end 
      end
    end


    # Never trust parameters from the scary internet, only allow the white list through.
    def vendor_params
     @vendor_params ||= params.permit(:name, :email, :mobile, :password, :dealership_id, :vendor_id, :user_id, :drop_time, :status, :driver_id, :remember_digest, :booking_id, :jobs, :available, :pickup_time, :offer_name, :expires_at, :terms, :description, :cost, :array, :manufacturer_id)
    end

    def check_booking
      @booking = Booking.find_by(id: vendor_params[:booking_id])
      if @booking
        @dealerships = @vendor.dealerships        
        if !@dealerships.ids.include?(@booking.dealership_id)
          respond_to do |format|        
            format.json { render json: {code: 0, msg: "Invalid access"}}
          end
        end
      else
        respond_to do |format|
          format.json { render json: {code: 0, msg: "Booking not found"}}
        end
      end      
    end

    def check_fields
      if vendor_params[:offer_name] && vendor_params[:terms] && vendor_params[:description] && vendor_params[:expires_at]
        if vendor_params[:offer_name].empty? || vendor_params[:terms].empty? || vendor_params[:description].empty? || vendor_params[:expires_at].empty?
          respond_to do |format|
            format.json { render json: {code: 0, msg: "one of the parameter is empty."}}
          end           
        end
      else
        respond_to do |format|
          format.json { render json: {code: 0, msg: "one of the parameter is not present."}}
        end        
      end
    end

    def check_offer_on
      if vendor_params[:array] && vendor_params[:manufacturer_id] 
        respond_to do |format|
          format.json { render json: {code: 0, msg: "Add a manufacturer or a dealership."}}
        end           
      elsif vendor_params[:array]
        if vendor_params[:array].empty?
          respond_to do |format|
            format.json { render json: {code: 0, msg: "Add dealerships."}}
          end  
        end           
      elsif vendor_params[:manufacturer_id]
        if vendor_params[:manufacturer_id].empty?
          respond_to do |format|
            format.json { render json: {code: 0, msg: "Add manufacturer."}}
          end 
        end
      else
        respond_to do |format|
          format.json { render json: {code: 0, msg: "No manufacturer or dealerships added."}}
        end 
      end
    end  

    def check_status
      if !Booking.statuses.include?(vendor_params[:status])
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Invalid booking status." }}
        end
      end       
    end
end
