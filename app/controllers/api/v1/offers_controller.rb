class Api::V1::OffersController < Api::V1::ApiController
  before_action :set_offer, only: [:show, :edit, :update, :destroy]

  # GET /offers
  # GET /offers.json
  def index
    @offers = Offer.where(deactivated: false)
    if !@offers.empty?        
      render json:{ code: 1,response: {  msg:"Listing all offers.", offers: @offers.as_json(only: ['id','name', 'image', 'description', 'terms'])}}
    else
      render json:{ code: 0,response:{ msg: "No offers available yet." }}
    end
  end

  def check_offer
    @offer= Offer.find_by(name: offer_params[:name].upcase)
    if @offer && !@offer.deactivated
      render json:{ code: 1,response:{ msg:"Offer is valid."}}
    else
       render json:{code: 0,response:{ msg:"Offer is not valid."}}
    end
  end

  private

    def offer_params
      params.permit(:name, :image, :expires_at)
    end  

end
