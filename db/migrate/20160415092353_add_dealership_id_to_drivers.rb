class AddDealershipIdToDrivers < ActiveRecord::Migration
  def change
    add_reference :drivers, :dealership, index: true, foreign_key: true
  end
end
