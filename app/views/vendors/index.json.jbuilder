json.array!(@vendors) do |vendor|
  json.extract! vendor, :id, :name, :email, :mobile, :password_digest, :remember_digest, :dealership_id
  json.url vendor_url(vendor, format: :json)
end
