class Api::V1::ApiController < ApplicationController

	def secure_user_api
		@user = User.find_by(remember_digest: params[:remember_digest])
		if !@user
			render json:{code: 2, response:{ msg:"User not found." }}
		end
	end

end 