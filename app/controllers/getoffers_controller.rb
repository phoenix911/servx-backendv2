class GetoffersController < ApplicationController
  before_action :set_getoffer, only: [:show, :edit, :update, :destroy]

  # GET /getoffers
  # GET /getoffers.json
  def index
    @getoffers = Getoffer.all
  end

  # GET /getoffers/1
  # GET /getoffers/1.json
  def show
  end

  # GET /getoffers/new
  def new
    @getoffer = Getoffer.new
  end

  # GET /getoffers/1/edit
  def edit
  end

  # POST /getoffers
  # POST /getoffers.json
  def create
    @getoffer = Getoffer.new(getoffer_params)

    respond_to do |format|
      if @getoffer.save
        format.html { redirect_to @getoffer, notice: 'Getoffer was successfully created.' }
        format.json { render :show, status: :created, location: @getoffer }
      else
        format.html { render :new }
        format.json { render json: @getoffer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /getoffers/1
  # PATCH/PUT /getoffers/1.json
  def update
    respond_to do |format|
      if @getoffer.update(getoffer_params)
        format.html { redirect_to @getoffer, notice: 'Getoffer was successfully updated.' }
        format.json { render :show, status: :ok, location: @getoffer }
      else
        format.html { render :edit }
        format.json { render json: @getoffer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /getoffers/1
  # DELETE /getoffers/1.json
  def destroy
    @getoffer.destroy
    respond_to do |format|
      format.html { redirect_to getoffers_url, notice: 'Getoffer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_getoffer
      @getoffer = Getoffer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def getoffer_params
      params.require(:getoffer).permit(:manufacturer_id, :offer_id)
    end
end
