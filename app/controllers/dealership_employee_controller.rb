class DealershipEmployeeController < ApplicationController
 before_action :logged_in_vendor, only: [:create_sub_vendor, :remove_sub_vendor]	
 before_action :check_entity,     only: [:create_service_advisor, :remove_service_advisor, :create_driver, :remove_driver]	
 before_action :check_dealership,  only: [:create_service_advisor, :remove_service_advisor, :create_driver, :remove_driver] 

	def create_sub_vendor
		@sub_vendor= SubVendor.new(dealership_employee_params.except(:sub_vendor_id, :remember_digest, :driver_id))
		respond_to do |format|
      if @sub_vendor.save
        format.json { render json:{code: 1, msg: "Welcome to ServX!", vendor: @sub_vendor.as_json(only: ['name','id','remember_digest','mobile','email']) }}
      else
        format.json { render json:{code: 0, msg: @sub_vendor.errors.messages}}
      end
    end
	end

  def remove_sub_vendor
    @sub_vendor= SubVendor.find_by(id: dealership_employee_params[:sub_vendor_id])
    name=@sub_vendor.name
    @sub_vendor.deactivate
    respond_to do |format|
      format.json { render json:{ code: 1, msg: "Removed #{name}." }}
    end
  end

	def create_service_advisor
		@service_advisor= ServiceAdvisor.new(dealership_employee_params.except( :remember_digest, :driver_id, :service_advisor_id))
		respond_to do |format|
      if @service_advisor.save
        format.json { render json:{code: 1, msg: "Welcome to ServX!", vendor: @service_advisor.as_json(only: ['name','id','remember_digest','mobile','email']) }}
      else
        format.json { render json:{code: 0, msg: @service_advisor.errors.messages}}
      end
    end
	end

  def remove_service_advisor
    @service_advisor= ServiceAdvisor.find_by(id: dealership_employee_params[:service_advisor_id])
    name=@service_advisor.name
    @service_advisor.deactivate
    respond_to do |format|
      format.json { render json:{ code: 1, msg: "Removed #{name}." }}
    end
  end  

	def create_driver
		@driver= Driver.new(dealership_employee_params.except( :remember_digest, :driver_id, :service_advisor_id))
		respond_to do |format|
      if @driver.save
        format.json { render json:{code: 1, msg: "Welcome to ServX!", vendor: @driver.as_json(only: ['name','id','remember_digest','mobile','email']) }}
      else
        format.json { render json:{code: 0, msg: @driver.errors.messages}}
      end
    end
	end

  def remove_driver
    @driver= Driver.find_by(id: dealership_employee_params[:driver_id])
    name=@driver.name
    @driver.deactivate
    respond_to do |format|
      format.json { render json:{ code: 1, msg: "Removed #{name}." }}
    end
  end   

	private
		# Never trust parameters from the scary internet, only allow the white list through.
    def dealership_employee_params
      params.permit(:name, :email, :mobile, :password, :vendor_id, :sub_vendor_id, :remember_digest, :dealership_id, :driver_id, :service_advisor_id)
    end

    def check_entity
      if dealership_employee_params[:vendor_id] && dealership_employee_params[:sub_vendor_id]
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "Please use vendor id or sub vendor id." }}
        end          
    	elsif dealership_employee_params[:vendor_id] && !dealership_employee_params[:vendor_id].blank?
        @current_vendor = Vendor.find_by(id: dealership_employee_params[:vendor_id])
        if @current_vendor && @current_vendor.remember_digest == params[:remember_digest]
          @vendor = @current_vendor
        elsif @current_vendor && @current_vendor.remember_digest != params[:remember_digest]
          respond_to do |format|
            format.json { render json:{ code: 2, msg: "Please sign in." }}
          end
        else
          respond_to do |format|
            format.json { render json:{ code: 2, msg: "Admin not found." }}
          end                
        end
    	elsif dealership_employee_params[:sub_vendor_id] && !dealership_employee_params[:sub_vendor_id].blank?
        @current_sub_vendor = SubVendor.find_by(id: dealership_employee_params[:sub_vendor_id])
        if @current_sub_vendor && @current_sub_vendor.remember_digest == params[:remember_digest]
          @sub_vendor = @current_sub_vendor
        elsif @current_sub_vendor && @current_sub_vendor.remember_digest != params[:remember_digest]
          respond_to do |format|
            format.json { render json:{ code: 2, msg: "Please sign in." }}
          end
        else
          respond_to do |format|
            format.json { render json:{ code: 0, msg: "Sub Admin not found." }}
          end                
        end 
      elsif  (dealership_employee_params[:vendor_id].nil? && dealership_employee_params[:sub_vendor_id].nil?) || (dealership_employee_params[:vendor_id].blank? && dealership_employee_params[:sub_vendor_id].blank?)
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "No Admin present." }}
        end  
    	end
    end

    def check_dealership
      if dealership_employee_params[:vendor_id]
        dealerships=@vendor.dealerships
        if dealerships
          if !dealerships.include?(dealership_employee_params[:dealership_id])  
            respond_to do |format|
              format.json { render json:{ code: 1, msg: "This dealership does not belong to you." }}
            end
          end  
        else
          respond_to do |format|
            format.json { render json:{ code: 0, msg: "No dealerships found." }}
          end
        end
      elsif dealership_employee_params[:sub_vendor_id]
        dealership=@sub_vendor[:dealership_id]
        if dealership
          if dealership!=dealership_employee_params[:dealership_id] 
            respond_to do |format|
              format.json { render json:{ code: 1, msg: "This dealership doesn not belong to you." }}
            end
          end  
        else
          respond_to do |format|
            format.json { render json:{ code: 0, msg: "No dealerships found." }}
          end
        end    
      else
        respond_to do |format|
          format.json { render json:{ code: 0, msg: "no admin found. check" }}
        end          
      end         
    end
end
