class Api::V1::WashBookingsController < Api::V1::ApiController
before_action :secure_user_api,    only: [:wash_booking, :wash_bookings]
before_action :check_wash_package,    only: [:wash_booking]
	def wash_booking
		@wash_booking = @user.wash_bookings.build(wash_booking_params)
		if @wash_booking.save
      @user.last_active_update
      @wash_booking.generate_ref_code
      render json:{ code:1,response:{  msg: "Booking successfully created. Our executives will contact you shortly.", wash_booking: @wash_booking.as_json(only: ['ref_code'])}}
      @wash_booking.send_message_now(@user[:name], @user[:mobile])
      @wash_booking.send_message_to_admin(@user[:name], "9838148268", @user[:mobile])
    else
      render json:{code:0,response:{ msg:@wash_booking.errors.messages}}
    end	
	end

	def wash_bookings
		@wash_bookings=@user.wash_bookings
	  render json:{ code:1,response:{  msg: "All car wash bookings.", wash_bookings: @wash_bookings.as_json(only: ['ref_code', 'slot', 'pickup_date'])}}
	end

	def get_wash_packages
		@wash_packages=WashPackage.all
	  if !@wash_packages.empty?        
	    render json:{ code: 1,response: {  msg:"Listing all packages.", wash_packages: @wash_packages.as_json(only: ['id', 'name', 'model', 'cost', 'jobs'])}}
	  else
	    render json:{ code: 0,response:{ msg: "Pssst!! No Packages available yet." }}
	  end	
	end

	private

		def wash_booking_params
			params.permit(:wash_package_id, :address, :pickup_date, :slot)
		end

		def check_wash_package
			@wash_package=WashPackage.find_by(id: wash_booking_params[:wash_package_id])
      if @wash_package.nil?
        render json:{ code: 0,response:{ msg:"Package not found." }}
      end 
		end
end