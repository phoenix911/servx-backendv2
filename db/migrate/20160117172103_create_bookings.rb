class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.references :vehicle, index: true, foreign_key: true
      t.datetime :pickup_time
      t.datetime :drop_time
      t.boolean :urgent
      t.string :reference_code
      t.text :feedback
      t.text :jobs
      t.integer :status

      t.timestamps null: false
    end
  end
end
