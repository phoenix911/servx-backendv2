json.array!(@dealerships) do |dealership|
  json.extract! dealership, :id, :name, :contact_person, :latitude, :longitude, :activated, :email, :manufacturers_id
  json.url dealership_url(dealership, format: :json)
end
