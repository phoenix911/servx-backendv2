class Booking < ActiveRecord::Base
  belongs_to :user
  belongs_to :vehicle
  belongs_to :dealership
  belongs_to :offer
  belongs_to :package
  belongs_to :picker, class_name: 'Driver', foreign_key: 'pickup_driver'
  belongs_to :dropper, class_name: 'Driver', foreign_key: 'drop_driver'  
  #has_many :payment, dependent: :destroy
  has_one :review
  validates :user_id, presence: {message: "User not found."}
  validates :vehicle_id, presence: {message: "Vehicle not found."}
  validates :dealership_id, presence: {message: "Dealer not found."}
  validates :jobs, presence: {message: "No jobs/services selected."}, allow_nil: true
  validates :package_id, presence: {message: "No package selected."}, allow_nil: true
  #before_save :check_time
  validate :jobs_xor_package
  #before_create :send_mails
  enum status: ["To be confirmed", "Status confirmed", "A Driver/Service Advisor has been assigned for your scheduled booking","A Driver/Service Advisor is on his way to your address",
   "Your vehicle is on its way to the service centre", "Your vehicle has reached the service centre", "Vehicle servicing has started" ,"Job(s) completed", 
    "Your vehicle is on its way to your address", "Vehicle delivered. Payment due", "Payment received"]

  validates :address, presence: {message: "Address not found."}, length: { minimum: 4, message: "Address is not valid (minimum 4 characters)." }
  validates :pickup_date, presence: {message: "Pick up date not found."}
  validates :slot, presence: {message: "Time slot not found."}
  before_save :check_date
  attr_accessor :get_package

  def check_date
    if self.pickup_date <= Date.today
      self.errors.add(:pickup_date,"Select a date ahead from today's date.")
      return false
    elsif self.pickup_date > Date.today+2.months
      self.errors.add(:pickup_date,"Selected date can not be ahead of 2 months from current date.")
      return false
    end 
  end
  # def check_time
  #   if self.pickup_time < Time.zone.now+12.hours
  #     self.errors.add(:pickup_time,"Select a date other from today's date.")
  #     return false
  #   elsif self.pickup_time > Time.zone.now+2.months+1.day+12.hours
  #     self.errors.add(:pickup_time,"Selected date and time can not be ahead of 2 months from current time.")
  #     return false
  #   elsif self.pickup_time.seconds_since_midnight < 32399 || self.pickup_time.seconds_since_midnight > 62999
  #     self.errors.add(:pickup_time,"Please select a time between 9 am and 5:30 pm.")
  #     return false
  #   end 
  # end  

  # def send_mails
  #   @user=self.user
  #   @vehicle=self.vehicle
  #   dealership= Dealership.includes(:manufacturer).where(id: self[:dealership_id])
  #   manufacturer=dealership[0].manufacturer
  #   UserMailer.booking_detail_to_admin(self,@user,@vehicle,dealership[0]).deliver_now
  #   UserMailer.booking_detail(self,@user,@vehicle,dealership[0],manufacturer).deliver_now
  # end
  
  def send_message_now(name,mobile)
    msg= "Hi%20#{name}.%20Your%20Booking%20has%20been%20confirmed%28reference%2Dcode:#{self.ref_code}%29.%20We%20will%20call%20you%20back%20shortly."
    Net::HTTP.get(URI("http://trans.kapsystem.com/api/v3/index.php?method=sms&api_key=A261352e7111244296e599e1b25b3fd7d&to=#{mobile}&sender=ISERVX&message=#{msg}&format=json&custom=1,2&flash=0"))
  end

  def send_message_to_admin(name,mobile,user_mobile)
    msg= "#{name}, #{user_mobile} has booked a service. Please check mail!"
    Net::HTTP.get(URI("http://trans.kapsystem.com/api/v3/index.php?method=sms&api_key=A261352e7111244296e599e1b25b3fd7d&to=#{mobile}&sender=ISERVX&message=#{msg}&format=json&custom=1,2&flash=0"))    
  end

  def send_user_booking_mail(user,vehicle,dealership,manufacturer,vehiclemodel)
    UserMailer.booking_detail(self,user,vehicle,dealership,manufacturer,vehiclemodel).deliver_now
  end

  def send_admin_booking_mail(user,vehicle,dealership,vehiclemodel)
    UserMailer.booking_detail_to_admin(self,user,vehicle,dealership,vehiclemodel).deliver_now
  end

  def generate_code
    ('A'..'Z').to_a.sample(4).join + (0..9).to_a.sample(4).join
  end

  def generate_reference_code
    code=generate_code
    idInHex=self.id.to_s(16)
    update_attribute(:ref_code,"#{code}#{idInHex}")      
  end 

  def update_drop_time(time)
    update_column(:drop_time, time)
  end

  def update_pickup_driver(pickup_driver_id)
    # The reason of using update_column is if I use update_attribute it runs all the validations which doesn't let object save if pickup_time reaches current date. daem!!!
    update_column(:pickup_driver, pickup_driver_id)
  end

  def update_drop_driver(drop_driver_id)
    update_column(:drop_driver, drop_driver_id)
  end 

  def change_job_format
    oldJobs=eval(self.jobs)
    newJobs=""
    oldJobs.each do |job|
      newJobs+=job
      newJobs+="#"
    end
    self.jobs=newJobs
  end 

  def jobs_xor_package
    if jobs.present? && package_id.present?
      errors.add(:base, "Specify a job or a package, not both.")
    elsif jobs.nil? && package_id.nil?
      errors.add(:base, "Specify a job or a package.")
    end
  end
end