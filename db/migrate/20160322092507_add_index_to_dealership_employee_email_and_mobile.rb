class AddIndexToDealershipEmployeeEmailAndMobile < ActiveRecord::Migration
  def change
  	add_index :sub_vendors, :mobile, unique: true
  	add_index :sub_vendors, :email, unique: true
  	add_index :service_advisors, :mobile, unique: true
  	add_index :service_advisors, :email, unique: true  	
  	add_index :drivers, :mobile, unique: true
  	add_index :drivers, :email, unique: true  	
  end
end
