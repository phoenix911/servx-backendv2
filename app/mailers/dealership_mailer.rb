class DealershipMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.dealership_mailer.dealership_booking_detail.subject
  #
  def dealership_booking_detail
    @greeting = "Hi"

    mail to: "to@example.org"
  end
end
